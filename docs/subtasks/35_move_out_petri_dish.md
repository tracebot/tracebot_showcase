# Use Case Move out petri dish

## Contained in

* [Manual Finishing](../tasks/12_manual_finishing.md), as Move out petri dish, step 2

## Steps

| ID | name | object | operation_type |
| -- | ---- | ------ | -------------- |
| 1 |[Locate petri dish](../subsubtasks/vision/v00_locate_object.md) | [petri dish](../object/petri_dish.md) | vision |

