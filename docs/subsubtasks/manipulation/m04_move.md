# Skill Move _object_

* Required information: object

## Contained in

* [Fit canisters to drain](../../subtasks/05_fit_canisters_to_drain.md), as move canister over drain, step 2
  with object = [canister](../../object/canister.md)
* [Fit canisters to drain](../../subtasks/05_fit_canisters_to_drain.md), as move Canister 2 over drain, step 6
  with object = [canister 2](../../object/canister.md)
* [Insert tube into pump](../../subtasks/06_insert_tube_into_pump.md), as move Tube over pump head, step 4
  with object = [tube](../../object/tube.md)
* [Insert needle](../../subtasks/10_insert_needle.md), as move needle over object, step 3
  with object = [needle](../../object/needle.md)
* [Hold bottle upside down](../../subtasks/12_hold_bottle_upside_down.md), as move (rotate), step 3
  with object = [bottle](../../object/bottle.md)
* [Attach red plugs](../../subtasks/17_attach_red_plugs.md), as Move red plug over canister air vent, step 4
  with object = [red plug](../../object/red_plug.md)
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Move canister to expose outlet port, step 5
  with object = [canister](../../object/canister.md)
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Move canister over drain, step 10
  with object = [canister](../../object/canister.md)
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Move canister to expose outlet port, step 14
  with object = [canister](../../object/canister.md)
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Move canister over drain, step 19
  with object = [canister](../../object/canister.md)

## Description

Moves an object in-hand to the defined location.
