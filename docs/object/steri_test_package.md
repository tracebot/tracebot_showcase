# Item steri test package

Alternative names:

* tyveck foil
* pack corner

![](../images/objects/steri_test_package.jpg)
![](../images/objects/steri_test_package2.jpg)

## Description

* The steri-test package contains the steri-test kit plus additional consumables
* It can be opened or closed.
* `tyveck foil` is not exactly an alternative name, but rather a component of the package,
  which has to be removed to open the pack
* `pack_corner` is the place where the gripper should grab the pack to enable the opening of the pack later on.
