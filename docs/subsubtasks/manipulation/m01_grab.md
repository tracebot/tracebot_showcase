# Skill Grab _object_

* Required information: object

## Contained in

* [label petri dish](../../subtasks/01_label_petri_dish.md), as grab, step 2
  with object = [pen](../../object/pen.md)
* [label petri dish](../../subtasks/01_label_petri_dish.md), as grab, step 5
  with object = [petri dish](../../object/petri_dish.md)
* [close pen](../../subtasks/01_label_petri_dish.md), as grab, step 8.1
  with object = [pen cover](../../object/pen.md)
* [label petri dish](../../subtasks/01_label_petri_dish.md), as grab pteri dish from temporary position, step 9
  with object = [petri dish](../../object/petri_dish.md)
* [Open kit](../../subtasks/03_open_kit.md), as Grab, step 3
  with object = [steri test package](../../object/steri_test_package.md)
* [Open kit](../../subtasks/03_open_kit.md), as Grab (pull tab) corner, step 5
  with object = [pack corner](../../object/steri_test_package.md)
* [Unpack kit](../../subtasks/04_unpack_kit.md), as grab, step 3
  with object = [canister](../../object/canister.md)
* [Unpack kit](../../subtasks/04_unpack_kit.md), as grab, step 4
  with object = [tube](../../object/tube.md)
* [Fit canisters to drain](../../subtasks/05_fit_canisters_to_drain.md), as grab Canister 2, step 5
  with object = [canister 2](../../object/canister.md)
* [Insert tube into pump](../../subtasks/06_insert_tube_into_pump.md), as grab tube, step 2
  with object = [tube](../../object/tube.md)
* [Remove needle cap](../../subtasks/09_remove_needle_cap.md), as grab needle (without touching the cap), step 2
  with object = [needle](../../object/needle.md)
* [Remove needle cap](../../subtasks/09_remove_needle_cap.md), as Grab needle cap, step 4
  with object = [needle cap](../../object/needle.md)
* [Hold bottle upside down](../../subtasks/12_hold_bottle_upside_down.md), as grab, step 2
  with object = [bottle](../../object/bottle.md)
* [Open plug bag](../../subtasks/16_open_plug_bag.md), as Grab, step 2
  with object = [plug bag](../../object/red_plug.md)
* [Open plug bag](../../subtasks/16_open_plug_bag.md), as Grab, step 3
  with object = [plug bag](../../object/red_plug.md)
* [Attach red plugs](../../subtasks/17_attach_red_plugs.md), as Grab red plug, step 3
  with object = [red plug](../../object/red_plug.md)
* [Detach red plugs](../../subtasks/18_detach_red_plugs.md), as Grab red plug, step 2
  with object = [red plug](../../object/red_plug.md)
* [Pull out needle](../../subtasks/19_pull_out_needle.md), as Grab bottle, step 3
  with object = [bottle](../../object/bottle.md)
* [Pull out needle](../../subtasks/19_pull_out_needle.md), as Grab needle, step 5
  with object = [needle](../../object/needle.md)
* [Take sample](../../subtasks/20_take_sample.md), as Grab, step 2
  with object = [sample](../../object/sample.md)
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Grab canister, step 3
  with object = [canister](../../object/canister.md)
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Grab yellow plug, step 6
  with object = [yellow plug](../../object/yellow_plug.md)
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Grab canister, step 12
  with object = [canister](../../object/canister.md)
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Grab yellow plug, step 15
  with object = [yellow plug](../../object/yellow_plug.md)
* [Take bottle down](../../subtasks/14_take_bottle_down.md), as Grab bottle, step 2
  with object = [bottle](../../object/bottle.md)
* [Attach cut tube to canister air vent](../../subtasks/31_attach_cut_tube_to_canister_air_vent.md), as Grab, step 3
  with object = [tube](../../object/tube.md)
* [Attach cut tube to canister air vent](../../subtasks/31_attach_cut_tube_to_canister_air_vent.md), as Grab, step 7
  with object = [tube](../../object/tube.md)
* [Remove tube from pump](../../subtasks/33_remove_tube_from_pump.md), as Grab tube, step 3
  with object = [tube](../../object/tube.md)
* [Remove tube from pump](../../subtasks/33_remove_tube_from_pump.md), as Grab tube, step 4
  with object = [tube](../../object/tube.md)
* [Store canisters](../../subtasks/26_store_canisters.md), as Grab  canister, step 2
  with object = [canister](../../object/canister.md)
* [Store canisters](../../subtasks/26_store_canisters.md), as Grab  canister, step 6
  with object = [canister](../../object/canister.md)

## Description

This subsubtask combines robot + gripper/hand motions to grab an object.

- Object must be located beforehand
  - TBD: Should a precise pose estimation using the hand-mounted camera be included as part of this task or the locate task?
- Grasp (hand posture + robot-object relative pose) selection
  - TBD: Is the grasp selection part of this subsubtask or is the grasp given as an input?
  - TBD: Is the DT capable of providing an _optimal_ grasp or should the manipulation components be in charge of this?
