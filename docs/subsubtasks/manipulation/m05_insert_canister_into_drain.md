# Skill Insert canister into drain tray hole

## Contained in

* [fit canister 1 to drain](../../subtasks/05_fit_canisters_to_drain.md), as Insert canister into drain, step 3.1
* [fit canister 2 to drain](../../subtasks/05_fit_canisters_to_drain.md), as Insert canister into drain, step 7.1
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Insert canister into drain, step 11
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Insert canister into drain, step 20

## Description

Insert the canister, which is already gripped, into one of the drain tray holes.
