# Use Case Break vial

## Contained in

* [Sample transferring](../tasks/06_sample_transferring.md), as Break vial, step 3
* [Sample transferring](../tasks/06_sample_transferring.md), as Break vial, step 9
* [Sample transferring](../tasks/06_sample_transferring.md), as Break vial, step 15
* [Sample transferring](../tasks/06_sample_transferring.md), as Break vial, step 21
* [Sample transferring](../tasks/06_sample_transferring.md), as Break vial, step 27
* [Sample transferring](../tasks/06_sample_transferring.md), as Break vial, step 33
* [Sample transferring](../tasks/06_sample_transferring.md), as Break vial, step 39
* [Sample transferring](../tasks/06_sample_transferring.md), as Break vial, step 45
* [Sample transferring](../tasks/06_sample_transferring.md), as Break vial, step 51
* [Sample transferring](../tasks/06_sample_transferring.md), as Break vial, step 57
* [Sample transferring](../tasks/06_sample_transferring.md), as Break vial, step 63
* [Sample transferring](../tasks/06_sample_transferring.md), as Break vial, step 69
* [Sample transferring](../tasks/06_sample_transferring.md), as Break vial, step 75
* [Sample transferring](../tasks/06_sample_transferring.md), as Break vial, step 81
* [Sample transferring](../tasks/06_sample_transferring.md), as Break vial, step 87
* [Sample transferring](../tasks/06_sample_transferring.md), as Break vial, step 93
* [Sample transferring](../tasks/06_sample_transferring.md), as Break vial, step 99
* [Sample transferring](../tasks/06_sample_transferring.md), as Break vial, step 105
* [Sample transferring](../tasks/06_sample_transferring.md), as Break vial, step 111

## Steps

| ID | name | operation_type |
| -- | ---- | -------------- |
| 1 |[Break vial](../subsubtasks/manipulation/m09_break_vial.md) | manipulation |

