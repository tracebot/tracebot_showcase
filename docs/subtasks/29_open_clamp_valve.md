# Use Case Open clamp valve

## Description

Opening a tube is realized by opening the clamps attached to it.

## Contained in

* [Media Filling](../tasks/09_media_filling.md), as Open tube, step 13
  with object = [white sample clamp](../object/clamp.md)

## Steps

| ID | name | arm | object | operation_type |
| -- | ---- | --- | ------ | -------------- |
| 1 |[locate tube clamp valve](../subsubtasks/vision/v00_locate_object.md) | | _object_ | vision |
| 2 |[Open clamp valve](../subsubtasks/manipulation/m14_open_clamp.md) | 1 | | manipulation |

