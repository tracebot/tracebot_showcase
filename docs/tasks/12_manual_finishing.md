# Use Case Manual Finishing

## Description

Clean up and move out petri dish


![type:video](../videos/12_manual_finishing_rssl.mp4)

## Contained in

* [Sterility Testing](../root_index.md), as Manual finishing, step 12

## Steps

| ID | name | operation_type | time_abs | time_rel |
| -- | ---- | -------------- | -------- | -------- |
| 1 |[Clean](../subtasks/34_clean.md) |  | [16:21, 17:22] | [0:00, 1:01] |
| 2 |[Move out petri dish](../subtasks/35_move_out_petri_dish.md) | vision | [17:22, 18:28] | [1:01, 2:07] |

