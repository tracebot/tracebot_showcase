# Use Case Fit canisters to drain

## Contained in

* [Kit mounting](../tasks/03_kit_mounting.md), as Fit canister to drain, step 1

## Steps

| ID | name | arm | digital_twin | object | operation_type |
| -- | ---- | --- | ------------ | ------ | -------------- |
| 1 |[locate drain](../subsubtasks/vision/v00_locate_object.md) | | | [drain tray](../object/drain_tray.md) | vision |
| 2 |[move canister over drain](../subsubtasks/manipulation/m04_move.md) | 2 | | [canister](../object/canister.md) | manipulation |
| 3 |_fit canister 1 to drain_ | [1 + 2] arm 2 force feedback vision | | | manipulation, vision |
| 3.1 |[Insert canister into drain](../subsubtasks/manipulation/m05_insert_canister_into_drain.md) | arm 2, arm 2 force feedback | | | manipulation |
| 3.2 |[Locate tube](../subsubtasks/vision/v00_locate_object.md) | | | [tube](../object/tube.md) | vision |
| 3.3 |_Grab tube and move tube to temporary position out of the way_ | 1 |[POSE(canister 1)](../subsubtasks/digital_twin/dt01_pose.md) [REGION_COLOR_IMAGE(canister 1)](../subsubtasks/digital_twin/dt06_region_color_image.md) [VISUAL_EXPECTATION_MATCH(canister 1)](../subsubtasks/digital_twin/dt07_visual_expectation_match.md) | | manipulation, vision |
| 4 |[locate canister 2](../subsubtasks/vision/v00_locate_object.md) | | | [canister 2](../object/canister.md) | vision |
| 5 |[grab Canister 2](../subsubtasks/manipulation/m01_grab.md) | 2 |[ESTIMATED_ROBOT_PARAMETER(gripper)](../subsubtasks/digital_twin/dt11_estimated_robot_parameter.md) | [canister 2](../object/canister.md) | manipulation |
| 6 |[move Canister 2 over drain](../subsubtasks/manipulation/m04_move.md) | 1 | | [canister 2](../object/canister.md) | manipulation |
| 7 |_fit canister 2 to drain_ | arm 1 + 2, arm 2 force feedback, vision | | | manipulation, vision |
| 7.1 |[Insert canister into drain](../subsubtasks/manipulation/m05_insert_canister_into_drain.md) | arm 2 force feedback | | | manipulation |
| 7.2 |[Locate tube](../subsubtasks/vision/v00_locate_object.md) | | | [tube](../object/tube.md) | vision |
| 7.3 |_Grab Tube and move tube to temporary position out of the way_ | 1 |[POSE(canister 2)](../subsubtasks/digital_twin/dt01_pose.md) [REGION_COLOR_IMAGE(canister 2)](../subsubtasks/digital_twin/dt06_region_color_image.md) [VISUAL_EXPECTATION_MATCH(canister 2)](../subsubtasks/digital_twin/dt07_visual_expectation_match.md) | | manipulation, vision |

