# Welcome to the TraceBot usecase

This website contains the description of the `sterility testing` process which is being used within the TraceBot project.
The description is based on the analysis of videos of human operator performing the process.

## Content

* [usecase description](root_index.md), to discover all steps and substeps associated to the sterility testing process
* [usecase statistics](root_index_stats.md), to see some numbers on the process content (and remaining inconsistencies)
* [Items](item_list), to access to the different items being used in the process.

---

## Acknowledgements

<a href="https://www.tracebot.eu/">
  <img src="https://www.tracebot.eu/files/layout/img/TraceBOT%20Logo%202021%20RGB-01.png"
       alt="tracebot_logo" height="60" >
</a>

Supported by TraceBot : [Visit project website][tracebot_website]

<img src="http://eurobench2020.eu/wp-content/uploads/2018/02/euflag.png"
     alt="eu_flag" width="100" align="left" >

This project receives funding from the European Union‘s H2020-EU.2.1.1. INDUSTRIAL LEADERSHIP programme (grant agreement No 101017089).

The opinions and arguments expressed reflect only the author‘s view and
reflect in no way the European Commission‘s opinions.
The European Commission is not responsible for any use that may be made
of the information it contains.

[tracebot_logo]: https://www.tracebot.eu/files/layout/img/TraceBOT%20Logo%202021%20RGB-01.png
[tracebot_website]: https://www.tracebot.eu "Go to website"

