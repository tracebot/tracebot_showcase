# Skill COMPONENT_REGION

* Required information: object

## Contained in

* [Attach cut tube to canister air vent](../../subtasks/31_attach_cut_tube_to_canister_air_vent.md), as Locate canister, step 1_1
  with object = [tube](../../object/tube.md)
* [Attach cut tube to canister air vent](../../subtasks/31_attach_cut_tube_to_canister_air_vent.md), as Locate canister, step 5_1
  with object = [tube](../../object/tube.md)
* [Remove tube from pump](../../subtasks/33_remove_tube_from_pump.md), as Undefined, step 1_1
  with object = [tube](../../object/tube.md)

## Description

Estimate 3D Interest Volume of a component in world/robot coords.
We hypothesize 'Regions of Interest' for object interactions:

1. Where is the graspable area of the needle, based on its pose?
2. How should the ROI look like given the belief and the actions?
   Render it as a photorealistic synthetic percept to be able to compare it with the real world sensor data.
