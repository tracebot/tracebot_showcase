# Skill REGION

* Required information: object

## Contained in

* [Insert tube into pump](../../subtasks/06_insert_tube_into_pump.md), as grab tube, step 2_3
  with object = [pump head](../../object/pump_head.md)
* [Insert tube into pump](../../subtasks/06_insert_tube_into_pump.md), as locate pump head, step 3_2
  with object = [pump head](../../object/pump_head.md)
* [Close pump](../../subtasks/07_close_pump.md), as press close pump button, step 2_2
  with object = [pump head](../../object/pump_head.md)
* [Remove needle cap](../../subtasks/09_remove_needle_cap.md), as grab needle (without touching the cap), step 2_2
  with object = [needle cap](../../object/needle.md)
* [Attach cut tube to canister air vent](../../subtasks/31_attach_cut_tube_to_canister_air_vent.md), as Locate canister, step 1_2
  with object = [tube](../../object/tube.md)
* [Attach cut tube to canister air vent](../../subtasks/31_attach_cut_tube_to_canister_air_vent.md), as Locate canister, step 5_2
  with object = [tube](../../object/tube.md)
* [Open pump](../../subtasks/32_open_pump.md), as press pump button, step 1_2
  with object = [pump head](../../object/pump_head.md)
* [Remove tube from pump](../../subtasks/33_remove_tube_from_pump.md), as Undefined, step 1_2
  with object = [tube](../../object/tube.md)

## Description

Estimate 3D Interest Volume in world/robot coords.
We hypothesize 'Regions of Interest' for object interactions:

1. Where should the red cap be located after fitting it?
2. How should the ROI look like given the belief and the actions? Render it as a photorealistic synthetic percept to be able to compare it with the real world sensor data.
