# Skill COLOR_IMAGE

## Description

Get synthetic color image of the estimated belief for augmenting real sensory information or comparing real sensory information with virtual real sensory information:

1. If an objects look occluded according to current real camera's viewpoint, then change camera's viewpoint in the digital twin and provide better images.
2. Compare real images and virtual images to check whether everything went as expected.
