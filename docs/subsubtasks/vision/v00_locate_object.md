# Skill locate object

* Required information: object

## Contained in

* [label petri dish](../../subtasks/01_label_petri_dish.md), as locate pen, step 1
  with object = [pen](../../object/pen.md)
* [label petri dish](../../subtasks/01_label_petri_dish.md), as locate petri dish, step 4
  with object = [petri dish](../../object/petri_dish.md)
* [Place petri dish](../../subtasks/02_place_petri_dish.md), as locate petri dish, step 1
  with object = [petri dish](../../object/petri_dish.md)
* [Open kit](../../subtasks/03_open_kit.md), as locate pack, step 1
  with object = [steri test package](../../object/steri_test_package.md)
* [Open kit](../../subtasks/03_open_kit.md), as locate corner, step 2
  with object = [pack corner](../../object/steri_test_package.md)
* [Unpack kit](../../subtasks/04_unpack_kit.md), as locate kit in open pack, step 1
  with object = [steri test kit](../../object/steri_test_kit.md)
* [Unpack kit](../../subtasks/04_unpack_kit.md), as locate tube, step 2
  with object = [tube](../../object/tube.md)
* [Fit canisters to drain](../../subtasks/05_fit_canisters_to_drain.md), as locate drain, step 1
  with object = [drain tray](../../object/drain_tray.md)
* [fit canister 1 to drain](../../subtasks/05_fit_canisters_to_drain.md), as Locate tube, step 3.2
  with object = [tube](../../object/tube.md)
* [Fit canisters to drain](../../subtasks/05_fit_canisters_to_drain.md), as locate canister 2, step 4
  with object = [canister 2](../../object/canister.md)
* [fit canister 2 to drain](../../subtasks/05_fit_canisters_to_drain.md), as Locate tube, step 7.2
  with object = [tube](../../object/tube.md)
* [Insert tube into pump](../../subtasks/06_insert_tube_into_pump.md), as locate tube grab position, step 1
  with object = [tube](../../object/tube.md)
* [Insert tube into pump](../../subtasks/06_insert_tube_into_pump.md), as locate pump head, step 3
  with object = [pump head](../../object/pump_head.md)
* [Close pump](../../subtasks/07_close_pump.md), as locate close pump button, step 1
  with object = [close pump button](../../object/pump.md)
* [Setup pump](../../subtasks/08_setup_pump.md), as locate pump knob, step 1
  with object = [pump knob](../../object/pump.md)
* [Setup pump](../../subtasks/08_setup_pump.md), as locate pump buttons, step 2
  with object = [pump buttons](../../object/pump.md)
* [Remove needle cap](../../subtasks/09_remove_needle_cap.md), as locate needle, step 1
  with object = [needle](../../object/needle.md)
* [Remove needle cap](../../subtasks/09_remove_needle_cap.md), as locate needle cap, step 3
  with object = [needle cap](../../object/needle.md)
* [Remove needle cap](../../subtasks/09_remove_needle_cap.md), as locate Pack, step 6
  with object = [steri test package](../../object/steri_test_package.md)
* [Insert needle](../../subtasks/10_insert_needle.md), as locate, step 1
  with object = _object_
* [Hold bottle upside down](../../subtasks/12_hold_bottle_upside_down.md), as locate bottle, step 1
  with object = [bottle](../../object/bottle.md)
* [Open plug bag](../../subtasks/16_open_plug_bag.md), as Locate plugs, step 1
  with object = [plugs](../../object/red_plug.md)
* [Attach red plugs](../../subtasks/17_attach_red_plugs.md), as Locate canister, step 1
  with object = [canister](../../object/canister.md)
* [Attach red plugs](../../subtasks/17_attach_red_plugs.md), as Locate plug, step 2
  with object = [red plug](../../object/red_plug.md)
* [Detach red plugs](../../subtasks/18_detach_red_plugs.md), as Locate plug, step 1
  with object = [red plug](../../object/red_plug.md)
* [Pull out needle](../../subtasks/19_pull_out_needle.md), as Locate needle, step 1
  with object = [needle](../../object/needle.md)
* [Pull out needle](../../subtasks/19_pull_out_needle.md), as Locate Bottle, step 2
  with object = [bottle](../../object/bottle.md)
* [Take sample](../../subtasks/20_take_sample.md), as Locate sample tray, step 1
  with object = [sample tray](../../object/sample.md)
* [Put down sample](../../subtasks/23_put_down_sample.md), as Locate sample tray, step 1
  with object = [sample tray](../../object/sample.md)
* [Clean up](../../subtasks/24_clean_up.md), as Locate bottle, step 1
  with object = [bottle](../../object/bottle.md)
* [Clean up](../../subtasks/24_clean_up.md), as Locate tube, step 2
  with object = [tube](../../object/tube.md)
* [Clean up](../../subtasks/24_clean_up.md), as Locate pack, step 3
  with object = [steri test package](../../object/steri_test_package.md)
* [Clean up](../../subtasks/24_clean_up.md), as Locate sample tray, step 4
  with object = [sample tray](../../object/sample.md)
* [Label Canisters](../../subtasks/25_label_canisters.md), as Locate canister, step 1
  with object = [canister](../../object/canister.md)
* [Label Canisters](../../subtasks/25_label_canisters.md), as Locate pen, step 2
  with object = [pen](../../object/pen.md)
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as locate canister, step 1
  with object = [canister](../../object/canister.md)
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as locate plugs, step 2
  with object = [plugs](../../object/red_plug.md)
* [Take bottle down](../../subtasks/14_take_bottle_down.md), as Locate bottle, step 1
  with object = [bottle](../../object/bottle.md)
* [Close Clamp valve](../../subtasks/28_close_clamp_valve.md), as locate tube clamp valve, step 1
  with object = _object_
* [Open clamp valve](../../subtasks/29_open_clamp_valve.md), as locate tube clamp valve, step 1
  with object = _object_
* [Cut tubes](../../subtasks/30_cut_tubes.md), as Locate tube, step 1
  with object = [tube](../../object/tube.md)
* [Cut tubes](../../subtasks/30_cut_tubes.md), as Locate scissors, step 2
  with object = [scissors](../../object/scissors.md)
* [Attach cut tube to canister air vent](../../subtasks/31_attach_cut_tube_to_canister_air_vent.md), as Locate canister, step 1
  with object = [canister](../../object/canister.md)
* [Attach cut tube to canister air vent](../../subtasks/31_attach_cut_tube_to_canister_air_vent.md), as Locate tube, step 2
  with object = [tube](../../object/tube.md)
* [Attach cut tube to canister air vent](../../subtasks/31_attach_cut_tube_to_canister_air_vent.md), as Locate canister, step 5
  with object = [canister](../../object/canister.md)
* [Attach cut tube to canister air vent](../../subtasks/31_attach_cut_tube_to_canister_air_vent.md), as Locate tube, step 6
  with object = [tube](../../object/tube.md)
* [Remove tube from pump](../../subtasks/33_remove_tube_from_pump.md), as Locate tube, step 2
  with object = [tube](../../object/tube.md)
* [Store canisters](../../subtasks/26_store_canisters.md), as Locate canister, step 1
  with object = [canister](../../object/canister.md)
* [Store canisters](../../subtasks/26_store_canisters.md), as Locate canister, step 5
  with object = [canister](../../object/canister.md)
* [Move out petri dish](../../subtasks/35_move_out_petri_dish.md), as Locate petri dish, step 1
  with object = [petri dish](../../object/petri_dish.md)

## Description

* tube: Should be able to run continuously, to be able to consider the cable as an obstacle in arm path planning
* needle: The needle will have to be localized both with and without the needle cap.
* pack: Not only must the pack be detected, but also the correct corner where the tab can be pulled.
  03_open-kit and 04-unpack-kit are being handled as if it was a v05-locate_pack.
  In 04, the pack is open, while in 03, it is not yet open, so the detection process will probably be different.
* sample tray: locating the entire sample tray and inferring the samples location from it should be sufficient for the subtasks (very challenging vision-wise)
* plugs: Use Digital Twin to reduce the search space.
  This task might need to use an on-gripper camera to more reliably estimate the pose of the plugs
* pen: The robot should probably perform the labeling differently (or not at all).
