# Use Case Wetting

## Description

The filter gets prepared for the sample filtering by running the pump with the needle attached to a new upside-down washing bottle, filling the canisters with washing medium. The bottle gets turned back down and the red plugs get attached. The pump runs (creating overpressure in the canisters) until there is no washing medium left in the canisters


![type:video](../videos/05_wetting_rssl.mp4)

## Contained in

* [Sterility Testing](../root_index.md), as Wetting, step 5

## Steps

| ID | name | arm | digital_twin | object | operation_type | time_abs | time_rel |
| -- | ---- | --- | ------------ | ------ | -------------- | -------- | -------- |
| 1 |[Start pump](../subtasks/11_start_pump.md) | | | |  | [4:55, 4:56] | [0:00, 0:01] |
| 2 |[Hold bottle upside down](../subtasks/12_hold_bottle_upside_down.md) | | | | manipulation, vision | [4:55, 5:04] | [0:00, 0:09] |
| 3 |[Wait](../subtasks/13_wait.md) | | | |  | [5:00, 5:01] | [0:05, 0:06] |
| 4 |[Take bottle down](../subsubtasks/manipulation/m02_place.md) | 2 |[POSE(bottle)](../subsubtasks/digital_twin/dt01_pose.md) [ESTIMATED_ROBOT_PARAMETER(gripper)](../subsubtasks/digital_twin/dt11_estimated_robot_parameter.md) | [bottle](../object/bottle.md) | manipulation | [5:04, 5:07] | [0:09, 0:12] |
| 5 |[Stop pump](../subtasks/15_stop_pump.md) | | | |  | [5:07, 5:09] | [0:12, 0:14] |
| 6 |[Open plug bag](../subtasks/16_open_plug_bag.md) | | | | manipulation, vision | [5:09, 5:14] | [0:14, 0:19] |
| 7 |[Attach red plugs](../subtasks/17_attach_red_plugs.md) | | | | manipulation, vision | [5:14, 5:21] | [0:19, 0:26] |
| 8 |[Start pump](../subtasks/11_start_pump.md) | | | |  | [5:20, 5:21] | [0:25, 0:26] |
| 9 |[Wait](../subtasks/13_wait.md) | | | |  | [5:21, 5:34] | [0:26, 0:39] |
| 10 |[Stop pump](../subtasks/15_stop_pump.md) | | | |  | [5:34, 5:35] | [0:39, 0:40] |
| 11 |[Detach red plugs](../subtasks/18_detach_red_plugs.md) | | | | manipulation, vision | [5:35, 5:39] | [0:40, 0:44] |


## Additional information

* pre_conditions:
  * Petri dish is placed
  * Kit package is opened
  * Canisters are mounted
  * Tube is mounted correctly
  * Pump is closed
  * Pump is setup
  * Tube is not damaged
  * Red plugs are in there intermediate position ready to be attached
  * Needle touches fresh washing medium
  * Bottle is not broken
  * Pump is not running
  * etc
* post_conditions:
  * Filter is conditioned correctly
  * No washing media in canisters
  * Red plugs not attached
  * etc

