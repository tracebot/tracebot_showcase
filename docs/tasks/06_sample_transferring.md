# Use Case Sample transferring

## Description

The sample is transferred from multiple vials into both canisters, one after the other.


![type:video](../videos/06_sample_transferring_rssl.mp4)

## Contained in

* [Sterility Testing](../root_index.md), as Sample transferring, step 6

## Steps

| ID | name | object | operation_type | time_abs | time_rel |
| -- | ---- | ------ | -------------- | -------- | -------- |
| 1 |[Pull out needle](../subtasks/19_pull_out_needle.md) | | manipulation, vision | [5:44, 5:48] | [0:00, 0:04] |
| 2 |[Take sample](../subtasks/20_take_sample.md) | | manipulation, vision | [5:53, 5:55] | [0:09, 0:11] |
| 3 |[Break vial](../subtasks/21_break_vial.md) | | manipulation | [5:55, 5:58] | [0:11, 0:14] |
| 4 |[Insert needle](../subtasks/10_insert_needle.md) | [vial](../object/vial.md) | manipulation, vision | [5:58, 5:59] | [0:14, 0:15] |
| 5 |[Pump sample](../subtasks/22_pump_sample.md) | | manipulation | [5:59, 6:04] | [0:15, 0:20] |
| 6 |[Pull out needle](../subtasks/19_pull_out_needle.md) | | manipulation, vision | [6:03, 6:04] | [0:19, 0:20] |
| 7 |[Put down sample](../subtasks/23_put_down_sample.md) | | manipulation, vision | [6:04, 6:05] | [0:20, 0:21] |
| 8 |[Take sample](../subtasks/20_take_sample.md) | | manipulation, vision | [6:06, 6:07] | [0:22, 0:23] |
| 9 |[Break vial](../subtasks/21_break_vial.md) | | manipulation | [6:07, 6:08] | [0:23, 0:24] |
| 10 |[Insert needle](../subtasks/10_insert_needle.md) | [vial](../object/vial.md) | manipulation, vision | [6:08, 6:10] | [0:24, 0:26] |
| 11 |[Pump sample](../subtasks/22_pump_sample.md) | | manipulation | [6:10, 6:13] | [0:26, 0:29] |
| 12 |[Pull out needle](../subtasks/19_pull_out_needle.md) | | manipulation, vision | [6:13, 6:14] | [0:29, 0:30] |
| 13 |[Put down sample](../subtasks/23_put_down_sample.md) | | manipulation, vision | [6:14, 6:15] | [0:30, 0:31] |
| 14 |[Take sample](../subtasks/20_take_sample.md) | | manipulation, vision | [6:14, 6:16] | [0:30, 0:32] |
| 15 |[Break vial](../subtasks/21_break_vial.md) | | manipulation | [6:16, 6:18] | [0:32, 0:34] |
| 16 |[Insert needle](../subtasks/10_insert_needle.md) | [vial](../object/vial.md) | manipulation, vision | [6:18, 6:19] | [0:34, 0:35] |
| 17 |[Pump sample](../subtasks/22_pump_sample.md) | | manipulation | [6:19, 6:23] | [0:35, 0:39] |
| 18 |[Pull out needle](../subtasks/19_pull_out_needle.md) | | manipulation, vision | [6:22, 6:23] | [0:38, 0:39] |
| 19 |[Put down sample](../subtasks/23_put_down_sample.md) | | manipulation, vision | [6:23, 6:24] | [0:39, 0:40] |
| 20 |[Take sample](../subtasks/20_take_sample.md) | | manipulation, vision | [6:24, 6:25] | [0:40, 0:41] |
| 21 |[Break vial](../subtasks/21_break_vial.md) | | manipulation | [6:25, 6:28] | [0:41, 0:44] |
| 22 |[Insert needle](../subtasks/10_insert_needle.md) | [vial](../object/vial.md) | manipulation, vision | [6:28, 6:29] | [0:44, 0:45] |
| 23 |[Pump sample](../subtasks/22_pump_sample.md) | | manipulation | [6:29, 6:33] | [0:45, 0:49] |
| 24 |[Pull out needle](../subtasks/19_pull_out_needle.md) | | manipulation, vision | [6:32, 6:33] | [0:48, 0:49] |
| 25 |[Put down sample](../subtasks/23_put_down_sample.md) | | manipulation, vision | [6:33, 6:35] | [0:49, 0:51] |
| 26 |[Take sample](../subtasks/20_take_sample.md) | | manipulation, vision | [6:35, 6:36] | [0:51, 0:52] |
| 27 |[Break vial](../subtasks/21_break_vial.md) | | manipulation | [6:36, 6:38] | [0:52, 0:54] |
| 28 |[Insert needle](../subtasks/10_insert_needle.md) | [vial](../object/vial.md) | manipulation, vision | [6:38, 6:39] | [0:54, 0:55] |
| 29 |[Pump sample](../subtasks/22_pump_sample.md) | | manipulation | [6:39, 6:42] | [0:55, 0:58] |
| 30 |[Pull out needle](../subtasks/19_pull_out_needle.md) | | manipulation, vision | [6:41, 6:42] | [0:57, 0:58] |
| 31 |[Put down sample](../subtasks/23_put_down_sample.md) | | manipulation, vision | [6:42, 6:44] | [0:58, 1:00] |
| 32 |[Take sample](../subtasks/20_take_sample.md) | | manipulation, vision | [6:45, 6:46] | [1:01, 1:02] |
| 33 |[Break vial](../subtasks/21_break_vial.md) | | manipulation | [6:46, 6:48] | [1:02, 1:04] |
| 34 |[Insert needle](../subtasks/10_insert_needle.md) | [vial](../object/vial.md) | manipulation, vision | [6:48, 6:49] | [1:04, 1:05] |
| 35 |[Pump sample](../subtasks/22_pump_sample.md) | | manipulation | [6:49, 6:52] | [1:05, 1:08] |
| 36 |[Pull out needle](../subtasks/19_pull_out_needle.md) | | manipulation, vision | [6:51, 6:52] | [1:07, 1:08] |
| 37 |[Put down sample](../subtasks/23_put_down_sample.md) | | manipulation, vision | [6:52, 6:54] | [1:08, 1:10] |
| 38 |[Take sample](../subtasks/20_take_sample.md) | | manipulation, vision | [6:55, 6:56] | [1:11, 1:12] |
| 39 |[Break vial](../subtasks/21_break_vial.md) | | manipulation | [6:56, 6:58] | [1:12, 1:14] |
| 40 |[Insert needle](../subtasks/10_insert_needle.md) | [vial](../object/vial.md) | manipulation, vision | [6:58, 6:59] | [1:14, 1:15] |
| 41 |[Pump sample](../subtasks/22_pump_sample.md) | | manipulation | [6:59, 7:02] | [1:15, 1:18] |
| 42 |[Pull out needle](../subtasks/19_pull_out_needle.md) | | manipulation, vision | [7:01, 7:02] | [1:17, 1:18] |
| 43 |[Put down sample](../subtasks/23_put_down_sample.md) | | manipulation, vision | [7:02, 7:04] | [1:18, 1:20] |
| 44 |[Take sample](../subtasks/20_take_sample.md) | | manipulation, vision | [7:05, 7:06] | [1:21, 1:22] |
| 45 |[Break vial](../subtasks/21_break_vial.md) | | manipulation | [7:06, 7:08] | [1:22, 1:24] |
| 46 |[Insert needle](../subtasks/10_insert_needle.md) | [vial](../object/vial.md) | manipulation, vision | [7:08, 7:09] | [1:24, 1:25] |
| 47 |[Pump sample](../subtasks/22_pump_sample.md) | | manipulation | [7:09, 7:12] | [1:25, 1:28] |
| 48 |[Pull out needle](../subtasks/19_pull_out_needle.md) | | manipulation, vision | [7:11, 7:12] | [1:27, 1:28] |
| 49 |[Put down sample](../subtasks/23_put_down_sample.md) | | manipulation, vision | [7:12, 7:14] | [1:28, 1:30] |
| 50 |[Take sample](../subtasks/20_take_sample.md) | | manipulation, vision | [7:15, 7:16] | [1:31, 1:32] |
| 51 |[Break vial](../subtasks/21_break_vial.md) | | manipulation | [7:28, 7:30] | [1:44, 1:46] |
| 52 |[Insert needle](../subtasks/10_insert_needle.md) | [vial](../object/vial.md) | manipulation, vision | [7:30, 7:31] | [1:46, 1:47] |
| 53 |[Pump sample](../subtasks/22_pump_sample.md) | | manipulation | [7:31, 7:34] | [1:47, 1:50] |
| 54 |[Pull out needle](../subtasks/19_pull_out_needle.md) | | manipulation, vision | [7:33, 7:34] | [1:49, 1:50] |
| 55 |[Put down sample](../subtasks/23_put_down_sample.md) | | manipulation, vision | [7:34, 7:36] | [1:50, 1:52] |
| 56 |[Take sample](../subtasks/20_take_sample.md) | | manipulation, vision | [7:37, 7:38] | [1:53, 1:54] |
| 57 |[Break vial](../subtasks/21_break_vial.md) | | manipulation | [7:38, 7:40] | [1:54, 1:56] |
| 58 |[Insert needle](../subtasks/10_insert_needle.md) | [vial](../object/vial.md) | manipulation, vision | [7:39, 7:40] | [1:55, 1:56] |
| 59 |[Pump sample](../subtasks/22_pump_sample.md) | | manipulation | [7:40, 7:43] | [1:56, 1:59] |
| 60 |[Pull out needle](../subtasks/19_pull_out_needle.md) | | manipulation, vision | [7:43, 7:44] | [1:59, 2:00] |
| 61 |[Put down sample](../subtasks/23_put_down_sample.md) | | manipulation, vision | [7:44, 7:46] | [2:00, 2:02] |
| 62 |[Take sample](../subtasks/20_take_sample.md) | | manipulation, vision | [7:47, 7:48] | [2:03, 2:04] |
| 63 |[Break vial](../subtasks/21_break_vial.md) | | manipulation | [7:48, 7:50] | [2:04, 2:06] |
| 64 |[Insert needle](../subtasks/10_insert_needle.md) | [vial](../object/vial.md) | manipulation, vision | [7:50, 7:51] | [2:06, 2:07] |
| 65 |[Pump sample](../subtasks/22_pump_sample.md) | | manipulation | [7:51, 7:54] | [2:07, 2:10] |
| 66 |[Pull out needle](../subtasks/19_pull_out_needle.md) | | manipulation, vision | [7:54, 7:55] | [2:10, 2:11] |
| 67 |[Put down sample](../subtasks/23_put_down_sample.md) | | manipulation, vision | [7:55, 7:56] | [2:11, 2:12] |
| 68 |[Take sample](../subtasks/20_take_sample.md) | | manipulation, vision | [7:56, 7:57] | [2:12, 2:13] |
| 69 |[Break vial](../subtasks/21_break_vial.md) | | manipulation | [7:57, 8:00] | [2:13, 2:16] |
| 70 |[Insert needle](../subtasks/10_insert_needle.md) | [vial](../object/vial.md) | manipulation, vision | [8:01, 8:02] | [2:17, 2:18] |
| 71 |[Pump sample](../subtasks/22_pump_sample.md) | | manipulation | [8:02, 8:05] | [2:18, 2:21] |
| 72 |[Pull out needle](../subtasks/19_pull_out_needle.md) | | manipulation, vision | [8:05, 8:06] | [2:21, 2:22] |
| 73 |[Put down sample](../subtasks/23_put_down_sample.md) | | manipulation, vision | [8:06, 8:07] | [2:22, 2:23] |
| 74 |[Take sample](../subtasks/20_take_sample.md) | | manipulation, vision | [8:07, 8:08] | [2:23, 2:24] |
| 75 |[Break vial](../subtasks/21_break_vial.md) | | manipulation | [8:08, 8:09] | [2:24, 2:25] |
| 76 |[Insert needle](../subtasks/10_insert_needle.md) | [vial](../object/vial.md) | manipulation, vision | [8:09, 8:10] | [2:25, 2:26] |
| 77 |[Pump sample](../subtasks/22_pump_sample.md) | | manipulation | [8:10, 8:14] | [2:26, 2:30] |
| 78 |[Pull out needle](../subtasks/19_pull_out_needle.md) | | manipulation, vision | [8:14, 8:15] | [2:30, 2:31] |
| 79 |[Put down sample](../subtasks/23_put_down_sample.md) | | manipulation, vision | [8:15, 8:16] | [2:31, 2:32] |
| 80 |[Take sample](../subtasks/20_take_sample.md) | | manipulation, vision | [8:16, 8:17] | [2:32, 2:33] |
| 81 |[Break vial](../subtasks/21_break_vial.md) | | manipulation | [8:17, 8:19] | [2:33, 2:35] |
| 82 |[Insert needle](../subtasks/10_insert_needle.md) | [vial](../object/vial.md) | manipulation, vision | [8:19, 8:20] | [2:35, 2:36] |
| 83 |[Pump sample](../subtasks/22_pump_sample.md) | | manipulation | [8:20, 8:24] | [2:36, 2:40] |
| 84 |[Pull out needle](../subtasks/19_pull_out_needle.md) | | manipulation, vision | [8:24, 8:25] | [2:40, 2:41] |
| 85 |[Put down sample](../subtasks/23_put_down_sample.md) | | manipulation, vision | [8:25, 8:26] | [2:41, 2:42] |
| 86 |[Take sample](../subtasks/20_take_sample.md) | | manipulation, vision | [8:26, 8:27] | [2:42, 2:43] |
| 87 |[Break vial](../subtasks/21_break_vial.md) | | manipulation | [8:27, 8:29] | [2:43, 2:45] |
| 88 |[Insert needle](../subtasks/10_insert_needle.md) | [vial](../object/vial.md) | manipulation, vision | [8:29, 8:30] | [2:45, 2:46] |
| 89 |[Pump sample](../subtasks/22_pump_sample.md) | | manipulation | [8:30, 8:34] | [2:46, 2:50] |
| 90 |[Pull out needle](../subtasks/19_pull_out_needle.md) | | manipulation, vision | [8:34, 8:35] | [2:50, 2:51] |
| 91 |[Put down sample](../subtasks/23_put_down_sample.md) | | manipulation, vision | [8:35, 8:36] | [2:51, 2:52] |
| 92 |[Take sample](../subtasks/20_take_sample.md) | | manipulation, vision | [8:36, 8:37] | [2:52, 2:53] |
| 93 |[Break vial](../subtasks/21_break_vial.md) | | manipulation | [8:37, 8:39] | [2:53, 2:55] |
| 94 |[Insert needle](../subtasks/10_insert_needle.md) | [vial](../object/vial.md) | manipulation, vision | [8:39, 8:40] | [2:55, 2:56] |
| 95 |[Pump sample](../subtasks/22_pump_sample.md) | | manipulation | [8:40, 8:44] | [2:56, 3:00] |
| 96 |[Pull out needle](../subtasks/19_pull_out_needle.md) | | manipulation, vision | [8:44, 8:45] | [3:00, 3:01] |
| 97 |[Put down sample](../subtasks/23_put_down_sample.md) | | manipulation, vision | [8:45, 8:46] | [3:01, 3:02] |
| 98 |[Take sample](../subtasks/20_take_sample.md) | | manipulation, vision | [8:46, 8:47] | [3:02, 3:03] |
| 99 |[Break vial](../subtasks/21_break_vial.md) | | manipulation | [8:47, 8:49] | [3:03, 3:05] |
| 100 |[Insert needle](../subtasks/10_insert_needle.md) | [vial](../object/vial.md) | manipulation, vision | [8:49, 8:50] | [3:05, 3:06] |
| 101 |[Pump sample](../subtasks/22_pump_sample.md) | | manipulation | [8:50, 8:54] | [3:06, 3:10] |
| 102 |[Pull out needle](../subtasks/19_pull_out_needle.md) | | manipulation, vision | [8:54, 8:55] | [3:10, 3:11] |
| 103 |[Put down sample](../subtasks/23_put_down_sample.md) | | manipulation, vision | [8:55, 8:56] | [3:11, 3:12] |
| 104 |[Take sample](../subtasks/20_take_sample.md) | | manipulation, vision | [8:56, 8:57] | [3:12, 3:13] |
| 105 |[Break vial](../subtasks/21_break_vial.md) | | manipulation | [9:07, 9:09] | [3:23, 3:25] |
| 106 |[Insert needle](../subtasks/10_insert_needle.md) | [vial](../object/vial.md) | manipulation, vision | [9:09, 9:10] | [3:25, 3:26] |
| 107 |[Pump sample](../subtasks/22_pump_sample.md) | | manipulation | [9:10, 9:14] | [3:26, 3:30] |
| 108 |[Pull out needle](../subtasks/19_pull_out_needle.md) | | manipulation, vision | [9:14, 9:15] | [3:30, 3:31] |
| 109 |[Put down sample](../subtasks/23_put_down_sample.md) | | manipulation, vision | [9:15, 9:16] | [3:31, 3:32] |
| 110 |[Take sample](../subtasks/20_take_sample.md) | | manipulation, vision | [9:16, 9:17] | [3:32, 3:33] |
| 111 |[Break vial](../subtasks/21_break_vial.md) | | manipulation | [9:17, 9:19] | [3:33, 3:35] |
| 112 |[Insert needle](../subtasks/10_insert_needle.md) | [vial](../object/vial.md) | manipulation, vision | [9:19, 9:20] | [3:35, 3:36] |
| 113 |[Pump sample](../subtasks/22_pump_sample.md) | | manipulation | [9:20, 9:24] | [3:36, 3:40] |
| 114 |[Pull out needle](../subtasks/19_pull_out_needle.md) | | manipulation, vision | [9:24, 9:25] | [3:40, 3:41] |
| 115 |[Put down sample](../subtasks/23_put_down_sample.md) | | manipulation, vision | [9:25, 9:26] | [3:41, 3:42] |
| 116 |[Insert needle](../subtasks/10_insert_needle.md) | [vial](../object/vial.md) | manipulation, vision | [9:34, 9:37] | [3:50, 3:53] |
| 117 |[Clean up](../subtasks/24_clean_up.md) | | vision | [9:43, 9:49] | [3:59, 4:05] |

