# Use Case Open plug bag

## Contained in

* [Wetting](../tasks/05_wetting.md), as Open plug bag, step 6

## Steps

| ID | name | arm | object | operation_type |
| -- | ---- | --- | ------ | -------------- |
| 1 |[Locate plugs](../subsubtasks/vision/v00_locate_object.md) | | [plugs](../object/red_plug.md) | vision |
| 2 |[Grab](../subsubtasks/manipulation/m01_grab.md) | 1 | [plug bag](../object/red_plug.md) | manipulation |
| 3 |[Grab](../subsubtasks/manipulation/m01_grab.md) | 2 | [plug bag](../object/red_plug.md) | manipulation |
| 4 |[Pull plug bag](../subsubtasks/manipulation/m03_pull.md) | 2 | [plug bag](../object/red_plug.md) | manipulation |
| 5 |[Drop plugs into package](../subsubtasks/manipulation/m17_drop_plugs_into_package.md) | 1 | | manipulation |

