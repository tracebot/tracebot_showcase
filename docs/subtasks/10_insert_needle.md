# Use Case Insert needle

## Description

Task generic to the object in which the needle can be inserted.
possibility seen: task-04: washing bottle, task-09: media-filling, task-06: vial
m07 was proposed for task-04, m04 for the others


## Contained in

* [NeedlePreparation](../tasks/04_needle_preparation.md), as Insert needle, step 2
  with object = [washing bottle](../object/bottle.md)
* [Sample transferring](../tasks/06_sample_transferring.md), as Insert needle, step 4
  with object = [vial](../object/vial.md)
* [Sample transferring](../tasks/06_sample_transferring.md), as Insert needle, step 10
  with object = [vial](../object/vial.md)
* [Sample transferring](../tasks/06_sample_transferring.md), as Insert needle, step 16
  with object = [vial](../object/vial.md)
* [Sample transferring](../tasks/06_sample_transferring.md), as Insert needle, step 22
  with object = [vial](../object/vial.md)
* [Sample transferring](../tasks/06_sample_transferring.md), as Insert needle, step 28
  with object = [vial](../object/vial.md)
* [Sample transferring](../tasks/06_sample_transferring.md), as Insert needle, step 34
  with object = [vial](../object/vial.md)
* [Sample transferring](../tasks/06_sample_transferring.md), as Insert needle, step 40
  with object = [vial](../object/vial.md)
* [Sample transferring](../tasks/06_sample_transferring.md), as Insert needle, step 46
  with object = [vial](../object/vial.md)
* [Sample transferring](../tasks/06_sample_transferring.md), as Insert needle, step 52
  with object = [vial](../object/vial.md)
* [Sample transferring](../tasks/06_sample_transferring.md), as Insert needle, step 58
  with object = [vial](../object/vial.md)
* [Sample transferring](../tasks/06_sample_transferring.md), as Insert needle, step 64
  with object = [vial](../object/vial.md)
* [Sample transferring](../tasks/06_sample_transferring.md), as Insert needle, step 70
  with object = [vial](../object/vial.md)
* [Sample transferring](../tasks/06_sample_transferring.md), as Insert needle, step 76
  with object = [vial](../object/vial.md)
* [Sample transferring](../tasks/06_sample_transferring.md), as Insert needle, step 82
  with object = [vial](../object/vial.md)
* [Sample transferring](../tasks/06_sample_transferring.md), as Insert needle, step 88
  with object = [vial](../object/vial.md)
* [Sample transferring](../tasks/06_sample_transferring.md), as Insert needle, step 94
  with object = [vial](../object/vial.md)
* [Sample transferring](../tasks/06_sample_transferring.md), as Insert needle, step 100
  with object = [vial](../object/vial.md)
* [Sample transferring](../tasks/06_sample_transferring.md), as Insert needle, step 106
  with object = [vial](../object/vial.md)
* [Sample transferring](../tasks/06_sample_transferring.md), as Insert needle, step 112
  with object = [vial](../object/vial.md)
* [Sample transferring](../tasks/06_sample_transferring.md), as Insert needle, step 116
  with object = [vial](../object/vial.md)
* [Media Filling](../tasks/09_media_filling.md), as Insert needle, step 4
  with object = [media bottle](../object/bottle.md)
* [Media Filling](../tasks/09_media_filling.md), as Insert needle, step 11
  with object = [media bottle](../object/bottle.md)

## Steps

| ID | name | arm | digital_twin | object | operation_type |
| -- | ---- | --- | ------------ | ------ | -------------- |
| 1 |[locate](../subsubtasks/vision/v00_locate_object.md) | | | _object_ | vision |
| 2 |_hold bottle still_ | 2 |[ESTIMATED_ROBOT_PARAMETER(gripper)](../subsubtasks/digital_twin/dt11_estimated_robot_parameter.md) [POSE(object)](../subsubtasks/digital_twin/dt01_pose.md) | | manipulation |
| 3 |[move needle over object](../subsubtasks/manipulation/m04_move.md) | 1 | | [needle](../object/needle.md) | manipulation |
| 4 |[insert needle into object](../subsubtasks/manipulation/m07_insert_needle_into_bottle.md) | [ Arm 1 (+ 2), vision, arm 1 + 2 force feedback] |[REGION_COLOR_IMAGE(needle)](../subsubtasks/digital_twin/dt06_region_color_image.md) [POSE(needle)](../subsubtasks/digital_twin/dt01_pose.md) [VISUAL_EXPECTATION_MATCH(needle)](../subsubtasks/digital_twin/dt07_visual_expectation_match.md) | | manipulation |

