# Skill Insert Bottle into Bottle Holder

* Required information: object

## Contained in

* [Move bottle into holder](../../subtasks/36_move_bottle_to_holder.md), as Place Bottle on bottle holder, step 2
  with object = [bottle](../../object/bottle.md)

## Description

Assuming the bottle is in the gripper, the placement of the bottle in the holder is performed.
This operation is considered more complex that a standard place operation,
as the bottle is somehow inserted into the central hole of he holder.
Also contact with the holder must be avoided during the process.
The bottle is placed as required parameter, as several bottles are being handled.