# Use Case label petri dish

## Contained in

* [manual_preparation](../tasks/01_manual_preparation.md), as Label petri dish, step 1

## Steps

| ID | name | arm | object | operation_type |
| -- | ---- | --- | ------ | -------------- |
| 1 |[locate pen](../subsubtasks/vision/v00_locate_object.md) | | [pen](../object/pen.md) | vision |
| 2 |[grab](../subsubtasks/manipulation/m01_grab.md) | 1 | [pen](../object/pen.md) | manipulation |
| 3 |_open pen_ | [1, 2] | [pen](../object/pen.md) | manipulation |
| 3.1 |_pull pen and cover apart_ | [1, 2] | [pen](../object/pen.md) | manipulation |
| 3.2 |[place](../subsubtasks/manipulation/m02_place.md) | 2 | [pen](../object/pen.md) | manipulation |
| 3.3 |_hold open pen_ | 1 | | |
| 4 |[locate petri dish](../subsubtasks/vision/v00_locate_object.md) | | [petri dish](../object/petri_dish.md) | vision |
| 5 |[grab](../subsubtasks/manipulation/m01_grab.md) | 2 | [petri dish](../object/petri_dish.md) | manipulation |
| 6 |_label petri dish_ | [1, 2] | | manipulation |
| 7 |[place temporarily](../subsubtasks/manipulation/m02_place.md) | 2 | [petri dish](../object/petri_dish.md) | manipulation |
| 8 |_close pen_ | [1, 2] | | manipulation |
| 8.1 |[grab](../subsubtasks/manipulation/m01_grab.md) | 2 | [pen cover](../object/pen.md) | manipulation |
| 8.2 |_push pen cover over pen tip_ | [1, 2] | [pen cover](../object/pen.md) | manipulation |
| 8.3 |_put down pen_ | [1] | [pen](../object/pen.md) | manipulation |
| 9 |[grab pteri dish from temporary position](../subsubtasks/manipulation/m01_grab.md) | 2 | [petri dish](../object/petri_dish.md) | manipulation |

