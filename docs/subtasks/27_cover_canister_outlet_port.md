# Use Case Cover canister outlet port

## Contained in

* [Media Filling](../tasks/09_media_filling.md), as Cover canister outlet port, step 1

## Steps

| ID | name | arm | digital_twin | object | operation_type |
| -- | ---- | --- | ------------ | ------ | -------------- |
| 1 |[locate canister](../subsubtasks/vision/v00_locate_object.md) | | | [canister](../object/canister.md) | vision |
| 2 |[locate plugs](../subsubtasks/vision/v00_locate_object.md) | | | [plugs](../object/red_plug.md) | vision |
| 3 |[Grab canister](../subsubtasks/manipulation/m01_grab.md) | 1 |[ESTIMATED_ROBOT_PARAMETER(gripper)](../subsubtasks/digital_twin/dt11_estimated_robot_parameter.md) | [canister](../object/canister.md) | manipulation |
| 4 |[Detach canister](../subsubtasks/manipulation/m11_detach_canister.md) | 1 |[POSE(canister)](../subsubtasks/digital_twin/dt01_pose.md) | [canister](../object/canister.md) | manipulation |
| 5 |[Move canister to expose outlet port](../subsubtasks/manipulation/m04_move.md) | 1 | | [canister](../object/canister.md) | manipulation |
| 6 |[Grab yellow plug](../subsubtasks/manipulation/m01_grab.md) | 2 | | [yellow plug](../object/yellow_plug.md) | manipulation |
| 7 |_Hold still canister_ | 1 | | | |
| 8 |[Plug yellow cap](../subsubtasks/manipulation/m12_insert_yellow_plug_into_outlet_port.md) | 2 |[POSE(yellow plug)](../subsubtasks/digital_twin/dt01_pose.md) [REGION_COLOR_IMAGE(yellow plug)](../subsubtasks/digital_twin/dt06_region_color_image.md) [VISUAL_EXPECTATION_MATCH(yellow plug)](../subsubtasks/digital_twin/dt07_visual_expectation_match.md) | | manipulation |
| 9 |_Stop hold still canister_ | 1 | | | |
| 10 |[Move canister over drain](../subsubtasks/manipulation/m04_move.md) | 2 |[POSE(canister)](../subsubtasks/digital_twin/dt01_pose.md) | [canister](../object/canister.md) | manipulation |
| 11 |[Insert canister into drain](../subsubtasks/manipulation/m05_insert_canister_into_drain.md) | arm 2, arm 2 force feedback |[POSE(canister)](../subsubtasks/digital_twin/dt01_pose.md) [REGION_COLOR_IMAGE(drain_tray)](../subsubtasks/digital_twin/dt06_region_color_image.md) [VISUAL_EXPECTATION_MATCH(drain_tray)](../subsubtasks/digital_twin/dt07_visual_expectation_match.md) | | manipulation |
| 12 |[Grab canister](../subsubtasks/manipulation/m01_grab.md) | 1 |[ESTIMATED_ROBOT_PARAMETER(gripper)](../subsubtasks/digital_twin/dt11_estimated_robot_parameter.md) | [canister](../object/canister.md) | manipulation |
| 13 |[Detach canister](../subsubtasks/manipulation/m11_detach_canister.md) | 1 |[POSE(canister)](../subsubtasks/digital_twin/dt01_pose.md) | | manipulation |
| 14 |[Move canister to expose outlet port](../subsubtasks/manipulation/m04_move.md) | 1 | | [canister](../object/canister.md) | manipulation |
| 15 |[Grab yellow plug](../subsubtasks/manipulation/m01_grab.md) | 2 | | [yellow plug](../object/yellow_plug.md) | manipulation |
| 16 |_Hold still canister_ | 1 | | | |
| 17 |[Plug yellow cap](../subsubtasks/manipulation/m12_insert_yellow_plug_into_outlet_port.md) | 2 |[POSE(yellow plug)](../subsubtasks/digital_twin/dt01_pose.md) [REGION_COLOR_IMAGE(yellow plug)](../subsubtasks/digital_twin/dt06_region_color_image.md) [VISUAL_EXPECTATION_MATCH(yellow plug)](../subsubtasks/digital_twin/dt07_visual_expectation_match.md) | [yellow plug](../object/yellow_plug.md) | manipulation |
| 18 |_Stop hold still canister_ | 1 | | | |
| 19 |[Move canister over drain](../subsubtasks/manipulation/m04_move.md) | 2 |[POSE(canister)](../subsubtasks/digital_twin/dt01_pose.md) | [canister](../object/canister.md) | manipulation |
| 20 |[Insert canister into drain](../subsubtasks/manipulation/m05_insert_canister_into_drain.md) | 2 force feedback |[POSE(canister)](../subsubtasks/digital_twin/dt01_pose.md) [REGION_COLOR_IMAGE(drain_tray)](../subsubtasks/digital_twin/dt06_region_color_image.md) [VISUAL_EXPECTATION_MATCH(drain_tray)](../subsubtasks/digital_twin/dt07_visual_expectation_match.md) | [canister](../object/canister.md) | manipulation |

