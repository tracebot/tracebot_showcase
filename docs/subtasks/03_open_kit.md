# Use Case Open kit

## Contained in

* [Kit unpacking](../tasks/02_kit_unpacking.md), as Open kit, step 1

## Steps

| ID | name | arm | digital_twin | object | operation_type |
| -- | ---- | --- | ------------ | ------ | -------------- |
| 1 |[locate pack](../subsubtasks/vision/v00_locate_object.md) | | | [steri test package](../object/steri_test_package.md) | vision |
| 2 |[locate corner](../subsubtasks/vision/v00_locate_object.md) | | | [pack corner](../object/steri_test_package.md) | vision |
| 3 |[Grab](../subsubtasks/manipulation/m01_grab.md) | 2 | | [steri test package](../object/steri_test_package.md) | manipulation |
| 4 |[Grab](../subsubtasks/manipulation/m02_place.md) | so Arm 1 holds the pack without touching the tyveck foil Arm 1 (+ 2) |[POSE(steri test package)](../subsubtasks/digital_twin/dt01_pose.md) | [steri test package](../object/steri_test_package.md) | manipulation |
| 5 |[Grab (pull tab) corner](../subsubtasks/manipulation/m01_grab.md) | Arm 2 (+ 1) | | [pack corner](../object/steri_test_package.md) | manipulation |
| 6 |[pull](../subsubtasks/manipulation/m03_pull.md) | Arm 2 (+ 1) | | [pack corner](../object/steri_test_package.md) | manipulation |
| 7 |_dispose pack_ | arm 1 +2 | | | manipulation |
| 7.1 |[place](../subsubtasks/manipulation/m02_place.md) | 1 | | [steri test package](../object/steri_test_package.md) | manipulation |
| 7.2 |[place](../subsubtasks/manipulation/m02_place.md) | 2 | | [tyveck foil](../object/steri_test_package.md) | manipulation |


## Additional information

| Item | Description |
| ---- | ------------|
| Task | Open pack |
| Object manipulated | pack tyveck foil |
| Challenge | * Flexible material * Item 2 |
| Arms | 2 |
| Perception | visual localisation (pack, pack corner),<br> force feedback (pull-tab corner) |
| Verification | foil removed,<br> kit not damaged |
| System state before | foil not damaged,<br> kit not damaged|
| System state after | kit completely present,<br> kit not damaged |

