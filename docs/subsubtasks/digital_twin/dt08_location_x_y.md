# Skill LOCATION(X,Y)

## Description

Is object X usually located in Y? Common sense is an essential feature of cognition.

1. Where is a canister usually located? (In the steritest kit)
2. Where can i find the bottles with the media? (In the rack. On the table)

Presents in:
In most of the tasks, the system could also be asked about the usual storage location of an item.
For example: LOCATION(RedCap, PlugBag), LOCATION(MediaBottle, BottleTray), etc.
