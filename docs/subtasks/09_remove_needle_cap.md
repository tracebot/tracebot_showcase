# Use Case Remove needle cap

## Contained in

* [NeedlePreparation](../tasks/04_needle_preparation.md), as Remove needle cap, step 1

## Steps

| ID | name | arm | digital_twin | object | operation_type |
| -- | ---- | --- | ------------ | ------ | -------------- |
| 1 |[locate needle](../subsubtasks/vision/v00_locate_object.md) | | | [needle](../object/needle.md) | vision |
| 2 |[grab needle (without touching the cap)](../subsubtasks/manipulation/m01_grab.md) | 1 |[ESTIMATED_ROBOT_PARAMETER(gripper)](../subsubtasks/digital_twin/dt11_estimated_robot_parameter.md) [REGION(needle cap)](../subsubtasks/digital_twin/dt03_region.md) | [needle](../object/needle.md) | manipulation |
| 3 |[locate needle cap](../subsubtasks/vision/v00_locate_object.md) | | | [needle cap](../object/needle.md) | vision |
| 4 |[Grab needle cap](../subsubtasks/manipulation/m01_grab.md) | 2 |[REGION_COLOR_IMAGE(needle)](../subsubtasks/digital_twin/dt06_region_color_image.md) [POSE(needle cap)](../subsubtasks/digital_twin/dt01_pose.md) [VISUAL_EXPECTATION_MATCH(needle)](../subsubtasks/digital_twin/dt07_visual_expectation_match.md) | [needle cap](../object/needle.md) | manipulation |
| 5 |[Pull needle cap](../subsubtasks/manipulation/m03_pull.md) | 2 | | [needle cap](../object/needle.md) | manipulation |
| 6 |[locate Pack](../subsubtasks/vision/v00_locate_object.md) | | | [steri test package](../object/steri_test_package.md) | vision |
| 7 |[place needle cap in pack](../subsubtasks/manipulation/m02_place.md) | 2 | | [needle cap](../object/needle.md) | manipulation |

