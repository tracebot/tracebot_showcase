# Use Case Take bottle down

## Description

At this level it is unclear whether the `take_out` operation is necessary,
or whether it is implicitely embedded within the `grab` process.
We leave it for now, as the presence of the holder may require special motion
in this case.

## Contained in

* [Media Filling](../tasks/09_media_filling.md), as Take bottle down, step 2
* [Media Filling](../tasks/09_media_filling.md), as Take bottle down, step 9
* [Finishing](../tasks/11_finishing.md), as Take bottle down, step 3

## Steps

| ID | name | arm | digital_twin | object | operation_type |
| -- | ---- | --- | ------------ | ------ | -------------- |
| 1 |[Locate bottle](../subsubtasks/vision/v00_locate_object.md) | | | [bottle](../object/bottle.md) | vision |
| 2 |[Grab bottle](../subsubtasks/manipulation/m01_grab.md) | 2 |[ESTIMATED_ROBOT_PARAMETER(gripper)](../subsubtasks/digital_twin/dt11_estimated_robot_parameter.md) | [bottle](../object/bottle.md) | manipulation |
| 3 |[Take bottle out of the holder](../subsubtasks/manipulation/m19_take_out_bottle_holder.md) | 2 | | [bottle](../object/bottle.md) | manipulation |
| 4 |[Place bottle](../subsubtasks/manipulation/m02_place.md) | 2 |[POSE(bottle)](../subsubtasks/digital_twin/dt01_pose.md) [ESTIMATED_ROBOT_PARAMETER(gripper)](../subsubtasks/digital_twin/dt11_estimated_robot_parameter.md) | [bottle](../object/bottle.md) | manipulation |

