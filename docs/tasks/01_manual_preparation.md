# Use Case manual_preparation

## Description

The control petri dish is labeled and placed in the workbench


![type:video](../videos/01_manual_preparation_rssl.mp4)

## Contained in

* [Sterility Testing](../root_index.md), as Manual Preparation, step 1

## Steps

| ID | name | operation_type | time_abs | time_rel |
| -- | ---- | -------------- | -------- | -------- |
| 1 |[Label petri dish](../subtasks/01_label_petri_dish.md) | manipulation, vision | [3:35, 4:06] | [0:00, 0:31] |
| 2 |[Place petri dish](../subtasks/02_place_petri_dish.md) | manipulation, vision | [4:06, 4:11] | [0:31, 0:36] |

