# Use Case Move bottle into holder

## Contained in

* [washing](../tasks/08_washing.md), as Move bottle into holder, step 1
* [Media Filling](../tasks/09_media_filling.md), as Move Bottle on bottle holder, step 7
* [Media Filling](../tasks/09_media_filling.md), as Move Bottle on bottle holder, step 15

## Steps

| ID | name | object | operation_type |
| -- | ---- | ------ | -------------- |
| 1 |[Grab bottle](12_hold_bottle_upside_down.md) | | manipulation, vision |
| 2 |[Place Bottle on bottle holder](../subsubtasks/manipulation/m18_insert_holder_bottle.md) | [bottle](../object/bottle.md) | manipulation |

