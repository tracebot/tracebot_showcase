# Use Case washing

## Description

Put wash solution into holder and start pump, leaving the drain of the canisters open


![type:video](../videos/08_washing_rssl.mp4)

## Contained in

* [Sterility Testing](../root_index.md), as Washing, step 8

## Steps

| ID | name | operation_type | time_abs | time_rel |
| -- | ---- | -------------- | -------- | -------- |
| 1 |[Move bottle into holder](../subtasks/36_move_bottle_to_holder.md) | manipulation, vision | [10:50, 10:53] | [0:00, 0:03] |
| 2 |[Attach red plugs](../subtasks/17_attach_red_plugs.md) | manipulation, vision | [11:09, 11:14] | [0:19, 0:24] |
| 3 |[Stop pump](../subtasks/15_stop_pump.md) |  | [12:22, 12:24] | [1:32, 1:34] |
| 4 |[Detach red plugs](../subtasks/18_detach_red_plugs.md) | manipulation, vision | [12:24, 12:30] | [1:34, 1:40] |


## Additional information

3 steps were initially defined here at the end of the sequence, but the video cut required to move them to next step
