# Skill Insert yellow plug into outlet port

## Contained in

* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Plug yellow cap, step 8
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Plug yellow cap, step 17

## Description

Insert the yellow plug, which is already gripped, into one of the canisters' outlet port.
