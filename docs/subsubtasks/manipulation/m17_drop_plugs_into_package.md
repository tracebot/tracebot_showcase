# Skill Drop plugs into package

## Contained in

* [Open plug bag](../../subtasks/16_open_plug_bag.md), as Drop plugs into package, step 5

## Description

Assuming the plug is in the gripper, shaking movement should be generated to get the plugs falling into the pack.
Visual process may be involved before or during, to orientate correctly the open bag, and then to confirm plugs fall.
Also verification process should verify the plugs are into the package.
It is up to us to add these vision items into this skills, or as pre and post operations.