# Item list

Listing of all elements of the environment referenced.

| Name | image |
| ---- | ---- |
| [bottle](object/bottle.md) | ![bottle](images/objects/bottle_big.jpg) |
| [bottle holder](object/bottle_holder.md) | ![bottle holder](images/objects/bottle_holder.png) |
| [canister](object/canister.md) | ![canister](images/objects/canister.jpg) |
| [canister air vent](object/canister_air_vent.md) | ![canister air vent](images/objects/canister_air_vent.jpg) |
| [canister_outlet_port](object/canister_outlet_port.md) | ![canister_outlet_port](images/objects/canister_outlet_port.jpg) |
| [clamp](object/clamp.md) | ![clamp](images/objects/clamps.jpg) |
| [drain tray](object/drain_tray.md) | ![drain tray](images/objects/drain_tray.png) |
| [needle](object/needle.md) | ![needle](images/objects/needle.jpg) |
| [pen](object/pen.md) | |
| [petri dish](object/petri_dish.md) | ![petri dish](images/objects/petri_dish.png) |
| [pump](object/pump.md) | ![pump](images/objects/pump.png) |
| [pump head](object/pump_head.md) | ![pump head](images/objects/pump_head.png) |
| [red plug](object/red_plug.md) | ![red plug](images/objects/plug_red.jpg) |
| [robot](object/robot.md) | |
| [sample](object/sample.md) | |
| [scissors](object/scissors.md) | |
| [septum](object/septum.md) | ![septum](images/objects/stopper.png) |
| [steri test kit](object/steri_test_kit.md) | ![steri test kit](images/objects/steri_test_kit.jpg) |
| [steri test package](object/steri_test_package.md) | ![steri test package](images/objects/steri_test_package.jpg) |
| [tube](object/tube.md) | ![tube](images/objects/tube.jpg) |
| [vial](object/vial.md) | ![vial](images/objects/vial_break.jpg) |
| [yellow plug](object/yellow_plug.md) | ![yellow plug](images/objects/plug_yellow.jpg) |
