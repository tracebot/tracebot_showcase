# Use Case Kit mounting

## Description

The canisters are placed into the canister tray, the tube is inserted into the pump, which is closed and set up


![type:video](../videos/03_kit_mounting_rssl.mp4)

## Contained in

* [Sterility Testing](../root_index.md), as Kit mounting, step 3

## Steps

| ID | name | operation_type | time_abs | time_rel |
| -- | ---- | -------------- | -------- | -------- |
| 1 |[Fit canister to drain](../subtasks/05_fit_canisters_to_drain.md) | manipulation, vision | [4:19, 4:24] | [0:00, 0:05] |
| 2 |[Insert tube into pump](../subtasks/06_insert_tube_into_pump.md) | manipulation, vision | [4:24, 4:30] | [0:05, 0:11] |
| 3 |[Close pump](../subtasks/07_close_pump.md) | manipulation, vision | [4:30, 4:34] | [0:11, 0:15] |
| 4 |[Setup pump](../subtasks/08_setup_pump.md) | manipulation, vision | [4:34, 4:40] | [0:15, 0:21] |

