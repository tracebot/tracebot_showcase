# Use Case Pull out needle

## Contained in

* [Sample transferring](../tasks/06_sample_transferring.md), as Pull out needle, step 1
* [Sample transferring](../tasks/06_sample_transferring.md), as Pull out needle, step 6
* [Sample transferring](../tasks/06_sample_transferring.md), as Pull out needle, step 12
* [Sample transferring](../tasks/06_sample_transferring.md), as Pull out needle, step 18
* [Sample transferring](../tasks/06_sample_transferring.md), as Pull out needle, step 24
* [Sample transferring](../tasks/06_sample_transferring.md), as Pull out needle, step 30
* [Sample transferring](../tasks/06_sample_transferring.md), as Pull out needle, step 36
* [Sample transferring](../tasks/06_sample_transferring.md), as Pull out needle, step 42
* [Sample transferring](../tasks/06_sample_transferring.md), as Pull out needle, step 48
* [Sample transferring](../tasks/06_sample_transferring.md), as Pull out needle, step 54
* [Sample transferring](../tasks/06_sample_transferring.md), as Pull out needle, step 60
* [Sample transferring](../tasks/06_sample_transferring.md), as Pull out needle, step 66
* [Sample transferring](../tasks/06_sample_transferring.md), as Pull out needle, step 72
* [Sample transferring](../tasks/06_sample_transferring.md), as Pull out needle, step 78
* [Sample transferring](../tasks/06_sample_transferring.md), as Pull out needle, step 84
* [Sample transferring](../tasks/06_sample_transferring.md), as Pull out needle, step 90
* [Sample transferring](../tasks/06_sample_transferring.md), as Pull out needle, step 96
* [Sample transferring](../tasks/06_sample_transferring.md), as Pull out needle, step 102
* [Sample transferring](../tasks/06_sample_transferring.md), as Pull out needle, step 108
* [Sample transferring](../tasks/06_sample_transferring.md), as Pull out needle, step 114
* [Media Filling](../tasks/09_media_filling.md), as Pull out needle, step 3
* [Media Filling](../tasks/09_media_filling.md), as Pull out needle, step 10

## Steps

| ID | name | digital_twin | object | operation_type |
| -- | ---- | ------------ | ------ | -------------- |
| 1 |[Locate needle](../subsubtasks/vision/v00_locate_object.md) | | [needle](../object/needle.md) | vision |
| 2 |[Locate Bottle](../subsubtasks/vision/v00_locate_object.md) | | [bottle](../object/bottle.md) | vision |
| 3 |[Grab bottle](../subsubtasks/manipulation/m01_grab.md) | | [bottle](../object/bottle.md) | manipulation |
| 4 |_Hold still bottle_ | | | |
| 5 |[Grab needle](../subsubtasks/manipulation/m01_grab.md) |[REGION_COLOR_IMAGE(top, bottle)](../subsubtasks/digital_twin/dt06_region_color_image.md) [POSE(needle)](../subsubtasks/digital_twin/dt01_pose.md) | [needle](../object/needle.md) | manipulation |
| 6 |[Pull needle](../subsubtasks/manipulation/m03_pull.md) |[VISUAL_EXPECTATION_MATCH(bottle)](../subsubtasks/digital_twin/dt07_visual_expectation_match.md) | [needle](../object/needle.md) | manipulation |
| 7 |_Stop Hold still bottle_ | | | |
| 8 |_Release bottle_ | | | |

