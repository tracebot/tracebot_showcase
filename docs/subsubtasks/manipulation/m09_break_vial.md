# Skill Break vial

## Contained in

* [Break vial](../../subtasks/21_break_vial.md), as Break vial, step 1

## Description

With the vial _body_ grasped by one arm, fit it into the vial opening tool and break it's head.
