# Use Case Finishing

## Description

Unmount everything and tidy up the workbench, moving apart consumables


![type:video](../videos/11_finishing_rssl.mp4)

## Contained in

* [Sterility Testing](../root_index.md), as Finishing, step 11

## Steps

| ID | name | operation_type | time_abs | time_rel |
| -- | ---- | -------------- | -------- | -------- |
| 1 |[Open pump](../subtasks/32_open_pump.md) | manipulation | [15:41, 15:47] | [0:00, 0:06] |
| 2 |[Remove tube from pump](../subtasks/33_remove_tube_from_pump.md) | manipulation, vision | [15:45, 15:47] | [0:04, 0:06] |
| 3 |[Take bottle down](../subtasks/14_take_bottle_down.md) | manipulation, vision | [15:47, 15:48] | [0:06, 0:07] |
| 4 |[Clean up](../subtasks/24_clean_up.md) | vision | [15:48, 16:06] | [0:07, 0:25] |
| 5 |[Clean up](../subtasks/24_clean_up.md) | vision | [16:06, 16:14] | [0:25, 0:33] |
| 6 |[Store canisters](../subtasks/26_store_canisters.md) | manipulation, vision | [16:14, 16:21] | [0:33, 0:40] |

