# Skill Slide tube into pump head

## Contained in

* [Insert tube into pump](../../subtasks/06_insert_tube_into_pump.md), as Slide tube into pump head, step 5

## Description

With the tube being grasped by two arms and located on top of the pump head, slide it vertically, while possibly applying a lateral oscillatory motion, until fully inserted.
