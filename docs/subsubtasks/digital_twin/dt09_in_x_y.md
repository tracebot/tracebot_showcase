# Skill IN(X,Y)

## Description

Is object X currently in Y according to DT? Note that though an object may not be visible to robot sensors, information about such an object can be provided by the digital twin.

1. An object may be completely occluded by other ones.
2. An object can unexpectedly change its pose after interaction and disappears from expected pose.
