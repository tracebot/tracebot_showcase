# Use Case Sterility Testing

## Description

The sterility testing procedure starts by setting up of materials, the central pump, and the sterility kit (as well as the transfer kit) in an isolator. Afterwards the sample is pumped from the sample containment (either the sample directly, e.g., in a vial, or the sample is transferred to another bottle first using the transfer kit before the filtration begins) through a membrane (where the filtering takes place) into the canister. For the complete test first a washing medium and afterwards the samples to be tested are pumped through the membranes on the bottom of two canisters and each canister is then closed and filled by one of two grow media. Afterwards, the canisters can visually be inspected, and a cloudy appearance indicates contaminant growth (whereas the main goal is to not detect contamination). This finishes the process within the isolator and the canisters are transferred into incubators outside the isolator.



## Steps

| ID | name | operation_type | video |
| -- | ---- | -------------- | ----- |
| 1 |[Manual Preparation](tasks/01_manual_preparation.md) | manipulation, vision | ![type:video](videos/01_manual_preparation_rssl.mp4) |
| 2 |[Kit unpacking](tasks/02_kit_unpacking.md) | manipulation, vision | ![type:video](videos/02_kit_unpacking_rssl.mp4) |
| 3 |[Kit mounting](tasks/03_kit_mounting.md) | manipulation, vision | ![type:video](videos/03_kit_mounting_rssl.mp4) |
| 4 |[Needle preparation](tasks/04_needle_preparation.md) | manipulation, vision | ![type:video](videos/04_needle_preparation_rssl.mp4) |
| 5 |[Wetting](tasks/05_wetting.md) | manipulation, vision | ![type:video](videos/05_wetting_rssl.mp4) |
| 6 |[Sample transferring](tasks/06_sample_transferring.md) | manipulation, vision | ![type:video](videos/06_sample_transferring_rssl.mp4) |
| 7 |[Sample filtering](tasks/07_sample_filtering.md) | manipulation, vision | ![type:video](videos/07_sample_filtering_rssl.mp4) |
| 8 |[Washing](tasks/08_washing.md) | manipulation, vision | ![type:video](videos/08_washing_rssl.mp4) |
| 9 |[Media filling](tasks/09_media_filling.md) | manipulation, vision | ![type:video](videos/09_media_filling_rssl.mp4) |
| 10 |[Cutting and closing](tasks/10_cutting_and_closing.md) | manipulation, vision | ![type:video](videos/10_cutting_and_closing_rssl.mp4) |
| 11 |[Finishing](tasks/11_finishing.md) | manipulation, vision | ![type:video](videos/11_finishing_rssl.mp4) |
| 12 |[Manual finishing](tasks/12_manual_finishing.md) | vision | ![type:video](videos/12_manual_finishing_rssl.mp4) |

