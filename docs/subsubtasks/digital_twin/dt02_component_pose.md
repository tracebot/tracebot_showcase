# Skill COMPONENT_POSE

* Required information: object

## Description

Estimate the pose of a component given the current belief/state.
Estimating the pose of some affordant components such as lid or handle in articulated objects is tricky.
The digital twin addresses this issue by maintaining realistic models of objects in the world:

1. Given the pose of the bottle: Where is the lid supposed to be?
2. Given the pose of the pump and its state(rotated or not ?): Where is the slid of the pump currently?
