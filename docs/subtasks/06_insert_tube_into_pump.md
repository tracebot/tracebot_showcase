# Use Case Insert tube into pump

## Contained in

* [Kit mounting](../tasks/03_kit_mounting.md), as Insert tube into pump, step 2

## Steps

| ID | name | arm | digital_twin | object | operation_type |
| -- | ---- | --- | ------------ | ------ | -------------- |
| 1 |[locate tube grab position](../subsubtasks/vision/v00_locate_object.md) | | | [tube](../object/tube.md) | vision |
| 2 |[grab tube](../subsubtasks/manipulation/m01_grab.md) | 2 |[ESTIMATED_ROBOT_PARAMETER(gripper)](../subsubtasks/digital_twin/dt11_estimated_robot_parameter.md) [POSE(pump head)](../subsubtasks/digital_twin/dt01_pose.md) [REGION(pump head)](../subsubtasks/digital_twin/dt03_region.md) | [tube](../object/tube.md) | manipulation |
| 3 |[locate pump head](../subsubtasks/vision/v00_locate_object.md) | |[POSE(pump head)](../subsubtasks/digital_twin/dt01_pose.md) [REGION(pump head)](../subsubtasks/digital_twin/dt03_region.md) | [pump head](../object/pump_head.md) | vision |
| 4 |[move Tube over pump head](../subsubtasks/manipulation/m04_move.md) | [1, 2] | | [tube](../object/tube.md) | manipulation |
| 5 |[Slide tube into pump head](../subsubtasks/manipulation/m06_slide_tube_into_pump_head.md) | [1, 2] | | | manipulation |

