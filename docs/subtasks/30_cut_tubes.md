# Use Case Cut tubes

## Contained in

* [Cutting and filling](../tasks/10_cutting_and_closing.md), as Cut tubes, step 3

## Steps

| ID | name | object | operation_type |
| -- | ---- | ------ | -------------- |
| 1 |[Locate tube](../subsubtasks/vision/v00_locate_object.md) | [tube](../object/tube.md) | vision |
| 2 |[Locate scissors](../subsubtasks/vision/v00_locate_object.md) | [scissors](../object/scissors.md) | vision |

