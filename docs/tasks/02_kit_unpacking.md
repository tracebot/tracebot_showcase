# Use Case Kit unpacking

## Description

The sterility kit is opened and unpacked


![type:video](../videos/02_kit_unpacking_rssl.mp4)

## Contained in

* [Sterility Testing](../root_index.md), as Kit unpacking, step 2

## Steps

| ID | name | operation_type | time_abs | time_rel |
| -- | ---- | -------------- | -------- | -------- |
| 1 |[Open kit](../subtasks/03_open_kit.md) | manipulation, vision | [4:11, 4:16] | [0:00, 0:05] |
| 2 |[Unpack kit](../subtasks/04_unpack_kit.md) | manipulation, vision | [4:16, 4:19] | [0:05, 0:08] |

