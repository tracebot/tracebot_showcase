# Skill Take bottle out of the Holder

* Required information: object

## Contained in

* [Take bottle down](../../subtasks/14_take_bottle_down.md), as Take bottle out of the holder, step 3
  with object = [bottle](../../object/bottle.md)

## Description

A bottle present in the holder is extracted.
This is not a regular pick, as contact in between the gripper and the surrounding holder must be avoided.
The bottle is placed as required parameter, as several bottles are being handled.
