# Use Case Unpack kit

## Contained in

* [Kit unpacking](../tasks/02_kit_unpacking.md), as Unpack kit, step 2

## Steps

| ID | name | arm | digital_twin | object | operation_type |
| -- | ---- | --- | ------------ | ------ | -------------- |
| 1 |[locate kit in open pack](../subsubtasks/vision/v00_locate_object.md) | | | [steri test kit](../object/steri_test_kit.md) | vision |
| 2 |[locate tube](../subsubtasks/vision/v00_locate_object.md) | | | [tube](../object/tube.md) | vision |
| 3 |[grab](../subsubtasks/manipulation/m01_grab.md) | 2 |[ESTIMATED_ROBOT_PARAMETER(gripper)](../subsubtasks/digital_twin/dt11_estimated_robot_parameter.md) | [canister](../object/canister.md) | manipulation |
| 4 |[grab](../subsubtasks/manipulation/m01_grab.md) | 1 |[ESTIMATED_ROBOT_PARAMETER(gripper)](../subsubtasks/digital_twin/dt11_estimated_robot_parameter.md) | [tube](../object/tube.md) | manipulation |
| 5 |[pull pack and tube out of pack](../subsubtasks/manipulation/m03_pull.md) | [1, 2] | | [tube](../object/tube.md) | manipulation |

