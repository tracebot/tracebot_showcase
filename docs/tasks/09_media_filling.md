# Use Case Media Filling

## Description

The media (red and green small bottles) is pumped and fills the two canisters, each canister with only one media.


![type:video](../videos/09_media_filling_rssl.mp4)

## Contained in

* [Sterility Testing](../root_index.md), as Media filling, step 9

## Steps

| ID | name | object | operation_type | time_abs | time_rel |
| -- | ---- | ------ | -------------- | -------- | -------- |
| 1 |[Cover canister outlet port](../subtasks/27_cover_canister_outlet_port.md) | | manipulation, vision | [12:31, 12:48] | [0:00, 0:17] |
| 2 |[Take bottle down](../subtasks/14_take_bottle_down.md) | | manipulation, vision | [12:54, 12:56] | [0:23, 0:25] |
| 3 |[Pull out needle](../subtasks/19_pull_out_needle.md) | | manipulation, vision | [12:56, 12:58] | [0:25, 0:27] |
| 4 |[Insert needle](../subtasks/10_insert_needle.md) | [media bottle](../object/bottle.md) | manipulation, vision | [12:58, 13:00] | [0:27, 0:29] |
| 5 |[Close tube](../subtasks/28_close_clamp_valve.md) | [white sample clamp](../object/clamp.md) | manipulation, vision | [13:04, 13:09] | [0:33, 0:38] |
| 6 |[Start pump](../subtasks/11_start_pump.md) | |  | [13:09, 13:11] | [0:38, 0:40] |
| 7 |[Move Bottle on bottle holder](../subtasks/36_move_bottle_to_holder.md) | | manipulation, vision | [13:11, 13:17] | [0:40, 0:46] |
| 8 |[Stop pump](../subtasks/15_stop_pump.md) | |  | [13:59, 14:01] | [1:28, 1:30] |
| 9 |[Take bottle down](../subtasks/14_take_bottle_down.md) | | manipulation, vision | [14:01, 14:03] | [1:30, 1:32] |
| 10 |[Pull out needle](../subtasks/19_pull_out_needle.md) | | manipulation, vision | [14:03, 14:05] | [1:32, 1:34] |
| 11 |[Insert needle](../subtasks/10_insert_needle.md) | [media bottle](../object/bottle.md) | manipulation, vision | [14:05, 14:10] | [1:34, 1:39] |
| 12 |[Close tube](../subtasks/28_close_clamp_valve.md) | [red sample clamp](../object/clamp.md) | manipulation, vision | [14:14, 14:15] | [1:43, 1:44] |
| 13 |[Open tube](../subtasks/29_open_clamp_valve.md) | [white sample clamp](../object/clamp.md) | manipulation, vision | [14:15, 14:17] | [1:44, 1:46] |
| 14 |[Start pump](../subtasks/11_start_pump.md) | |  | [14:17, 14:18] | [1:46, 1:47] |
| 15 |[Move Bottle on bottle holder](../subtasks/36_move_bottle_to_holder.md) | | manipulation, vision | [14:22, 14:24] | [1:51, 1:53] |
| 16 |[Stop pump](../subtasks/15_stop_pump.md) | |  | [15:05, 15:12] | [2:34, 2:41] |


## Additional information

The 3 first steps were initially defined in the previous sequence, but the video cut required to bring it here.

