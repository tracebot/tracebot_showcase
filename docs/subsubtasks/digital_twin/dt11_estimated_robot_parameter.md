# Skill ESTIMATED_ROBOT_PARAMETER

## Contained in

* [Unpack kit](../../subtasks/04_unpack_kit.md), as grab, step 3_1
* [Unpack kit](../../subtasks/04_unpack_kit.md), as grab, step 4_1
* [Fit canisters to drain](../../subtasks/05_fit_canisters_to_drain.md), as grab Canister 2, step 5_1
* [Insert tube into pump](../../subtasks/06_insert_tube_into_pump.md), as grab tube, step 2_1
* [Remove needle cap](../../subtasks/09_remove_needle_cap.md), as grab needle (without touching the cap), step 2_1
* [Insert needle](../../subtasks/10_insert_needle.md), as hold bottle still, step 2_1
* [Hold bottle upside down](../../subtasks/12_hold_bottle_upside_down.md), as grab, step 2_1
* [Wetting](../../tasks/05_wetting.md), as Take bottle down, step 4_2
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Grab canister, step 3_1
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Grab canister, step 12_1
* [Take bottle down](../../subtasks/14_take_bottle_down.md), as Grab bottle, step 2_1
* [Take bottle down](../../subtasks/14_take_bottle_down.md), as Place bottle, step 4_2
* [Attach cut tube to canister air vent](../../subtasks/31_attach_cut_tube_to_canister_air_vent.md), as Grab, step 3_1
* [Attach cut tube to canister air vent](../../subtasks/31_attach_cut_tube_to_canister_air_vent.md), as Grab, step 7_1
* [Remove tube from pump](../../subtasks/33_remove_tube_from_pump.md), as Grab tube, step 3_1
* [Remove tube from pump](../../subtasks/33_remove_tube_from_pump.md), as Grab tube, step 4_1
* [Store canisters](../../subtasks/26_store_canisters.md), as Grab  canister, step 2_1
* [Store canisters](../../subtasks/26_store_canisters.md), as Grab  canister, step 6_1

## Description

Get the current value of a robot parameter given the current belief/state.

1. After I have grasped object X: What is the configuration of the gripper? It shouldn't be fully closed after successfully grasping something.
