# Use Case Sterility Testing statistics

Switch to [use case description](root_index.md)

## Overall view

105 item in the use case:

* 87 item defined
* 45 processes
* 35 skills
* 7 inline definition
* 23 undefined
* 5 orphans

## Categories

 items with a category.

| Category | name |
| -------- | ---- |
| labelling | |
| | [label petri dish](subtasks/01_label_petri_dish.md) |
| | [Label Canisters](subtasks/25_label_canisters.md) |
| pick and place | |
| | [Place petri dish](subtasks/02_place_petri_dish.md) |
| | [Take sample](subtasks/20_take_sample.md) |
| | [Put down sample](subtasks/23_put_down_sample.md) |
| | [Clean up](subtasks/24_clean_up.md) |
| | [Move out petri dish](subtasks/35_move_out_petri_dish.md) |
| complex/special | |
| | [Open kit](subtasks/03_open_kit.md) |
| | [Unpack kit](subtasks/04_unpack_kit.md) |
| | [Open plug bag](subtasks/16_open_plug_bag.md) |
| | [Break vial](subtasks/21_break_vial.md) |
| insert/attach | |
| | [Fit canisters to drain](subtasks/05_fit_canisters_to_drain.md) |
| | [Insert needle](subtasks/10_insert_needle.md) |
| | [Attach red plugs](subtasks/17_attach_red_plugs.md) |
| | [Cover canister outlet port](subtasks/27_cover_canister_outlet_port.md) |
| tube handling | |
| | [Insert tube into pump](subtasks/06_insert_tube_into_pump.md) |
| | [Close Clamp valve](subtasks/28_close_clamp_valve.md) |
| | [Open clamp valve](subtasks/29_open_clamp_valve.md) |
| | [Cut tubes](subtasks/30_cut_tubes.md) |
| | [Attach cut tube to canister air vent](subtasks/31_attach_cut_tube_to_canister_air_vent.md) |
| | [Remove tube from pump](subtasks/33_remove_tube_from_pump.md) |
| button | |
| | [Close pump](subtasks/07_close_pump.md) |
| | [Setup pump](subtasks/08_setup_pump.md) |
| | [Start pump](subtasks/11_start_pump.md) |
| | [Stop pump](subtasks/15_stop_pump.md) |
| | [Pump sample](subtasks/22_pump_sample.md) |
| | [Open pump](subtasks/32_open_pump.md) |
| pull out /detach | |
| | [Remove needle cap](subtasks/09_remove_needle_cap.md) |
| | [Detach red plugs](subtasks/18_detach_red_plugs.md) |
| | [Pull out needle](subtasks/19_pull_out_needle.md) |
| | [Store canisters](subtasks/26_store_canisters.md) |
| bottle moving | |
| | [Hold bottle upside down](subtasks/12_hold_bottle_upside_down.md) |
| | [Move bottle into holder](subtasks/36_move_bottle_to_holder.md) |
| | [Take bottle down](subtasks/14_take_bottle_down.md) |

## Undefined processes

23 items are undefined

| Name | Referenced in |
| ---- | ------------- |
| pull pen and cover apart | [subtasks/01_label_petri_dish.md](subtasks/01_label_petri_dish.md) |
| hold open pen | [subtasks/01_label_petri_dish.md](subtasks/01_label_petri_dish.md) |
| label petri dish | [subtasks/01_label_petri_dish.md](subtasks/01_label_petri_dish.md) |
| push pen cover over pen tip | [subtasks/01_label_petri_dish.md](subtasks/01_label_petri_dish.md) |
| put down pen | [subtasks/01_label_petri_dish.md](subtasks/01_label_petri_dish.md) |
| open petri dish | [subtasks/02_place_petri_dish.md](subtasks/02_place_petri_dish.md) |
| cover petri dish | [subtasks/02_place_petri_dish.md](subtasks/02_place_petri_dish.md) |
| Grab tube and move tube to temporary position out of the way | [subtasks/05_fit_canisters_to_drain.md](subtasks/05_fit_canisters_to_drain.md) |
| Grab Tube and move tube to temporary position out of the way | [subtasks/05_fit_canisters_to_drain.md](subtasks/05_fit_canisters_to_drain.md) |
| press close pump button | [subtasks/07_close_pump.md](subtasks/07_close_pump.md) |
| read settings from gui | [subtasks/08_setup_pump.md](subtasks/08_setup_pump.md) |
| turn pump knob | [subtasks/08_setup_pump.md](subtasks/08_setup_pump.md) |
| push pump button | [subtasks/08_setup_pump.md](subtasks/08_setup_pump.md) |
| hold bottle still | [subtasks/10_insert_needle.md](subtasks/10_insert_needle.md) |
| Hold still bottle | [subtasks/19_pull_out_needle.md](subtasks/19_pull_out_needle.md) |
| Stop Hold still bottle | [subtasks/19_pull_out_needle.md](subtasks/19_pull_out_needle.md) |
| Release bottle | [subtasks/19_pull_out_needle.md](subtasks/19_pull_out_needle.md) |
| Hold still canister | [subtasks/27_cover_canister_outlet_port.md](subtasks/27_cover_canister_outlet_port.md) |
| Stop hold still canister | [subtasks/27_cover_canister_outlet_port.md](subtasks/27_cover_canister_outlet_port.md) |
| Hold still canister | [subtasks/27_cover_canister_outlet_port.md](subtasks/27_cover_canister_outlet_port.md) |
| Stop hold still canister | [subtasks/27_cover_canister_outlet_port.md](subtasks/27_cover_canister_outlet_port.md) |
| press pump button | [subtasks/32_open_pump.md](subtasks/32_open_pump.md) |
| Undefined | [subtasks/33_remove_tube_from_pump.md](subtasks/33_remove_tube_from_pump.md) |

## Process - skills with unmet requirements


## Orphans processes

5 items are orphans

* [automation/subsubtasks/digital_twin/dt02_component_pose.yaml](subsubtasks/digital_twin/dt02_component_pose.md)
* [automation/subsubtasks/digital_twin/dt05_color_image.yaml](subsubtasks/digital_twin/dt05_color_image.md)
* [automation/subsubtasks/digital_twin/dt08_location_x_y.yaml](subsubtasks/digital_twin/dt08_location_x_y.md)
* [automation/subsubtasks/digital_twin/dt09_in_x_y.yaml](subsubtasks/digital_twin/dt09_in_x_y.md)
* [automation/subsubtasks/digital_twin/dt10_on_x_y.yaml](subsubtasks/digital_twin/dt10_on_x_y.md)

## Unknown objects

4 are unknown.

* object in [subtasks/10_insert_needle.md](subtasks/10_insert_needle.md)
* object in [subtasks/10_insert_needle.md](subtasks/10_insert_needle.md)
* object in [subtasks/28_close_clamp_valve.md](subtasks/28_close_clamp_valve.md)
* object in [subtasks/29_open_clamp_valve.md](subtasks/29_open_clamp_valve.md)

## Unreferenced objects

4 are not referenced in the process.

* [bottle holder](object/bottle_holder.md)
* [canister air vent](object/canister_air_vent.md)
* [canister_outlet_port](object/canister_outlet_port.md)
* [septum](object/septum.md)
