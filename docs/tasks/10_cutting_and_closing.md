# Use Case Cutting and filling

## Description

Close both clamps and cut


![type:video](../videos/10_cutting_and_closing_rssl.mp4)

## Contained in

* [Sterility Testing](../root_index.md), as Cutting and closing, step 10

## Steps

| ID | name | object | operation_type | time_abs | time_rel |
| -- | ---- | ------ | -------------- | -------- | -------- |
| 1 |[Close tube at first canister](../subtasks/28_close_clamp_valve.md) | [red canister clamp](../object/clamp.md) | manipulation, vision | [15:12, 15:16] | [0:00, 0:04] |
| 2 |[Close tube at second canister](../subtasks/28_close_clamp_valve.md) | [white canister clamp](../object/clamp.md) | manipulation, vision | [15:16, 15:19] | [0:04, 0:07] |
| 3 |[Cut tubes](../subtasks/30_cut_tubes.md) | | vision | [15:19, 15:29] | [0:07, 0:17] |
| 4 |[Attach cut tube to canister air vent](../subtasks/31_attach_cut_tube_to_canister_air_vent.md) | | manipulation, vision | [15:29, 15:41] | [0:17, 0:29] |

