# Skill Insert red plug into air vent

## Contained in

* [Attach red plugs](../../subtasks/17_attach_red_plugs.md), as Insert red plug into air vent, step 5

## Description

Insert the red plug, which is already gripped, into one of the canisters' air vent.
