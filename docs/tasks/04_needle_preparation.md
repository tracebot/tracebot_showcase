# Use Case NeedlePreparation

## Description

The needle cap is removed and inserted into the bottle through the membrane.


![type:video](../videos/04_needle_preparation_rssl.mp4)

## Contained in

* [Sterility Testing](../root_index.md), as Needle preparation, step 4

## Steps

| ID | name | object | operation_type | time_abs | time_rel |
| -- | ---- | ------ | -------------- | -------- | -------- |
| 1 |[Remove needle cap](../subtasks/09_remove_needle_cap.md) | | manipulation, vision | [4:41, 4:44] | [0:00, 0:03] |
| 2 |[Insert needle](../subtasks/10_insert_needle.md) | [washing bottle](../object/bottle.md) | manipulation, vision | [4:44, 4:49] | [0:03, 0:08] |

