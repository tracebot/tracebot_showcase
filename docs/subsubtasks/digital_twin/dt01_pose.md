# Skill POSE

* Required information: object

## Contained in

* [Open kit](../../subtasks/03_open_kit.md), as Grab, step 4_1
  with object = [steri test package](../../object/steri_test_package.md)
* [fit canister 1 to drain](../../subtasks/05_fit_canisters_to_drain.md), as Grab tube and move tube to temporary position out of the way, step 3.3_1
  with object = [canister 1](../../object/canister.md)
* [fit canister 2 to drain](../../subtasks/05_fit_canisters_to_drain.md), as Grab Tube and move tube to temporary position out of the way, step 7.3_1
  with object = [canister 2](../../object/canister.md)
* [Insert tube into pump](../../subtasks/06_insert_tube_into_pump.md), as grab tube, step 2_2
  with object = [pump head](../../object/pump_head.md)
* [Insert tube into pump](../../subtasks/06_insert_tube_into_pump.md), as locate pump head, step 3_1
  with object = [pump head](../../object/pump_head.md)
* [Close pump](../../subtasks/07_close_pump.md), as press close pump button, step 2_1
  with object = [pump head](../../object/pump_head.md)
* [Remove needle cap](../../subtasks/09_remove_needle_cap.md), as Grab needle cap, step 4_2
  with object = [needle cap](../../object/needle.md)
* [Insert needle](../../subtasks/10_insert_needle.md), as hold bottle still, step 2_2
  with object = _object_
* [Insert needle](../../subtasks/10_insert_needle.md), as insert needle into object, step 4_2
  with object = [needle](../../object/needle.md)
* [Hold bottle upside down](../../subtasks/12_hold_bottle_upside_down.md), as move (rotate), step 3_1
  with object = [bottle](../../object/bottle.md)
* [Wetting](../../tasks/05_wetting.md), as Take bottle down, step 4_1
  with object = [bottle](../../object/bottle.md)
* [Attach red plugs](../../subtasks/17_attach_red_plugs.md), as Grab red plug, step 3_2
  with object = [red plug](../../object/red_plug.md)
* [Detach red plugs](../../subtasks/18_detach_red_plugs.md), as Grab red plug, step 2_2
  with object = [red plug](../../object/red_plug.md)
* [Pull out needle](../../subtasks/19_pull_out_needle.md), as Grab needle, step 5_2
  with object = [needle](../../object/needle.md)
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Detach canister, step 4_1
  with object = [canister](../../object/canister.md)
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Plug yellow cap, step 8_1
  with object = [yellow plug](../../object/yellow_plug.md)
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Move canister over drain, step 10_1
  with object = [canister](../../object/canister.md)
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Insert canister into drain, step 11_1
  with object = [canister](../../object/canister.md)
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Detach canister, step 13_1
  with object = [canister](../../object/canister.md)
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Plug yellow cap, step 17_1
  with object = [yellow plug](../../object/yellow_plug.md)
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Move canister over drain, step 19_1
  with object = [canister](../../object/canister.md)
* [Cover canister outlet port](../../subtasks/27_cover_canister_outlet_port.md), as Insert canister into drain, step 20_1
  with object = [canister](../../object/canister.md)
* [Take bottle down](../../subtasks/14_take_bottle_down.md), as Place bottle, step 4_1
  with object = [bottle](../../object/bottle.md)
* [Open pump](../../subtasks/32_open_pump.md), as press pump button, step 1_1
  with object = [pump head](../../object/pump_head.md)
* [Store canisters](../../subtasks/26_store_canisters.md), as Detach canister, step 3_1
  with object = [canister](../../object/canister.md)
* [Store canisters](../../subtasks/26_store_canisters.md), as Detach canister, step 7_1
  with object = [canister](../../object/canister.md)

## Description

Estimate the pose of an object given the current belief/state.
We understand the DT as a rich knowledge-base which can also be used as a subsymbolic forward model of robot actions.
This allows us to return for example estimated poses of relevant objects after manipulation actions:

1. I grasped a canister and commanded to move it our the drain.
   Is it really over there according to our forward model given the belief about the scene and the manipulation parameters
2. I pushed the canister into the tray with trajectory X.
  Given these motion parameters, what is the end pose of the canister after executing the action? Is it fully inserted or skewed?
