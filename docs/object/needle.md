# Item needle

Alternative names:

* needle cap

![](../images/objects/needle.jpg)

## Description

* the `needle cap` refers to the blue cap placed on the needle in the illustrative figure.
