# Skill Open clamp

## Contained in

* [Open clamp valve](../../subtasks/29_open_clamp_valve.md), as Open clamp valve, step 2

## Description

Press and slide finger on the clamp valve to release the lock.
Assumes the clamp pose is known.