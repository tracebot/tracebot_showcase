# Skill Place _object_

* Required information: object

## Contained in

* [open pen](../../subtasks/01_label_petri_dish.md), as place, step 3.2
  with object = [pen](../../object/pen.md)
* [label petri dish](../../subtasks/01_label_petri_dish.md), as place temporarily, step 7
  with object = [petri dish](../../object/petri_dish.md)
* [place and cover petri dish](../../subtasks/02_place_petri_dish.md), as place, step 3.1
  with object = [petri dish](../../object/petri_dish.md)
* [Open kit](../../subtasks/03_open_kit.md), as Grab, step 4
  with object = [steri test package](../../object/steri_test_package.md)
* [dispose pack](../../subtasks/03_open_kit.md), as place, step 7.1
  with object = [steri test package](../../object/steri_test_package.md)
* [dispose pack](../../subtasks/03_open_kit.md), as place, step 7.2
  with object = [tyveck foil](../../object/steri_test_package.md)
* [Remove needle cap](../../subtasks/09_remove_needle_cap.md), as place needle cap in pack, step 7
  with object = [needle cap](../../object/needle.md)
* [Wetting](../../tasks/05_wetting.md), as Take bottle down, step 4
  with object = [bottle](../../object/bottle.md)
* [Detach red plugs](../../subtasks/18_detach_red_plugs.md), as Place red plug, step 4
  with object = [red plug](../../object/red_plug.md)
* [Put down sample](../../subtasks/23_put_down_sample.md), as Place sample, step 2
  with object = [sample](../../object/sample.md)
* [Take bottle down](../../subtasks/14_take_bottle_down.md), as Place bottle, step 4
  with object = [bottle](../../object/bottle.md)
* [Store canisters](../../subtasks/26_store_canisters.md), as Place canister, step 4
  with object = [canister](../../object/canister.md)
* [Store canisters](../../subtasks/26_store_canisters.md), as Place canister, step 8
  with object = [canister](../../object/canister.md)

## Description

Put an object in hand on top of a flat surface.
- Object placement position must be provided
  - TBD: Is the DT capable of providing the appropriate placement position?
